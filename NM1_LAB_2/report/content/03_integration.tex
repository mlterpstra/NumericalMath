%!TEX root = ../report.tex
\section{Numerical integration}

\subsection{Number of subintervals}
The integral of the given function was approximated using the three methods.
\autoref{fig:int_error_comparison} shows the approximated error as a function of the number of subintervals.
It can be seen that the adaptive Simpson method is by far the best.
The composite trapezoidal method sometimes has a larger error than the adaptive trapezoidal method when using the same number of subintervals.
This is because the composite trapezoidal method adds new nodes uniformly, and not only where it is necessary, whereas the adaptive trapezoidal method does not add new nodes on intervals that have a low enough error in proportion to their width.

\begin{figure}[htb]
	\centering
	\includegraphics[width=0.5\columnwidth]{images/int_error_comparison}
	\caption{
		Comparison of the error of the three integration approximation methods.
	}
	\label{fig:int_error_comparison}
\end{figure}

\subsection{Efficiency}
\autoref{fig:int_efficiency_comparison} shows the computation time eacht of the three methods.
When using a tolerance of \(10^{-10}\) the adaptive trapezoidal method becomes more efficent than the composite trapezoidal method.
For this particular value of the tolerance, the numbers of intervals needed for attaining a certain error are shown in \autoref{fig:int_one_tol}.

\begin{figure}[htb]
	\centering
	\includegraphics[width=0.5\columnwidth]{images/int_efficiency_comparison}
	\caption{
		Comparison of the efficiency of the three integration approximation methods.
		The computation time is given in seconds.
	}
	\label{fig:int_efficiency_comparison}
\end{figure}

\begin{figure}[htb]
	\centering
	\includegraphics[width=0.5\columnwidth]{images/int_one_tol}
	\caption{
		Number of intervals needed for error values, with a tolerance of \(10^{-10}\)
	}
	\label{fig:int_one_tol}
\end{figure}

\subsection{Line thirty-seven}
Line 37 of \t{trap\_comp} reads:
\begin{minted}{matlab}
	intNew = int / 2 + h * sum(feval(f, newNodes, varargin{:}));
\end{minted}

This ensures that the function evaluations that were previously computed are reused.
The information that is reused is in the \t{int} variable, which is an approximation of the integral that is too coarse.

To fully explain what happens in line 37 we can consider a single interval from \(a\) to \(b\) that is bisected at the point \(c = \frac{a+b}{2}\).
The coarse approximation \(I_c\) is simply the area of the trapezoid:
\[
	\int\limits_a^b f(x)dx \approx I_c = \frac{h}{2} \left(
		f(a) + f(b)
	\right)
\]

The finer approximation \(I_f\) is the sum of two narrower trapezoids:
\begin{align}
	\int\limits_a^b f(x)dx \approx I_f &= \frac{h}{4} \left(
		f(a) + f(c)
	\right) +
	\frac{h}{4} \left(
		f(c) + f(b)
	\right)
	\nonumber
	\\
	&=
	\frac{h}{4}\left(
	f(a) + f(b)
	\right)
	+
	\frac{h}{2} f(c)
	\nonumber
	\\
	&=
	\frac{1}{2}I_c + \frac{h}{2} f(c).
	\nonumber
\end{align}

The first term corresponds to \mintinline{matlab}{int / 2}, and the second term corresponds to\\\mintinline{matlab}{h * sum(feval(f, newNodes, varargin{:}))}.
Note that in the code, \mintinline{matlab}{h} has already been divided by two before this statement is executed.
Also note that this principle is implemented recursively for all subintervals.

\subsection{Advantages}
The adaptive Trapezoidal formula has the advantage over the composite Trapezoidal formula that it only resamples the intervals where higher resolution is necessary (i.e. those regions where the error is high w.r.t. the current intervals). If there is one part of a function that is very detailed, the composite Trapezoidal formula will resample the entire function, which can be costly. The adaptive formula will only resamples those intervals where the error is too high.

\subsection{Finding the error}
One can compute \(\int_\alpha^\beta f(x)\d x\) using the composite Trapezoidal rule.
The error of the (regular) trapezoidal rule is given by:
\begin{equation}
	\label{eq:trap_err}
	E_t = -\frac{(\beta - \alpha) ^ 3}{12}f''(\xi)
\end{equation}
The error of the composite trapezoidal integration scheme is given by eq. 4.18:
\begin{equation}
	\label{eq:comp_trap_err}
	I(f) - I_t^c(f) = -\frac{\beta - \alpha}{12}H^2f''(\xi)
\end{equation}
If we compute this for a steplength \(H = \frac{\beta - \alpha}{2}\) this yields
\begin{align}
	\label{eq:comp_trap}
	\int_\alpha^\beta f(x)\d x - I_t^c(f) &= -\frac{\beta - \alpha}{12}\left(\frac{\beta - \alpha}{2}\right)^2f''(\eta)\\
										  &= -\frac{(\beta - \alpha)^3}{48}f''(\eta)
\end{align}
where \(\eta\) is a point in \(\lbrack \alpha, \beta\rbrack\) and is unequal to \(\xi\).

Subtracting \autoref{eq:comp_trap} from \autoref{eq:trap_err} yields an expression for \(\Delta I\), which is given by:
\begin{align}
	\Delta I &= -\frac{(\beta - \alpha)^3}{12}f''(\xi) - \left(-\frac{(\beta - \alpha)^3}{48}f''(\eta)\right) \\
			 &= \frac{4(\beta - \alpha)^3}{48}f''(\xi) + \frac{(\beta - \alpha)^3}{48}f''(\eta) \\
			 &= -\frac{3(\beta - \alpha)^3}{48}f''(\xi)
\end{align}
This is assuming \(f''(\eta) \simeq f''(\xi)\). This can subsequently be used for an expression \(f''(\eta)\). 
Rewriting the former equation yields
\begin{equation}
	\label{eq:feta}
	f''(\eta) = \Delta I \frac{-48}{3(\beta - \alpha)^3}
\end{equation}

Substituting this back into \autoref{eq:comp_trap} yields an approximation of the integral, which is:
\begin{align}
	\label{eq:eindelijkklaar}
	\int_\alpha^\beta f(x)\d x - I_t^c(f) &= -\frac{(\beta - \alpha)^3}{48}f''(\eta)\\
										  &\simeq -\frac{(\beta - \alpha)^3}{48}\Delta I \frac{-48}{3(\beta - \alpha)^3}\\
										  &= \frac{1}{3}\Delta I
\end{align}

\subsection{Changing the code}
To obtain the adaptive Trapezoidal formula from the adaptive Simpson method, some changes to the code had to be made. 
These were the following:
\begin{enumerate}
	\item The number of new intervals that is to be made when an interval is subdivided
	\item The weights of the nodes when fine and coarse estimates are made
	\item In the function \verb bisectIntervals  the weights and bisection scheme (in terms of number of points and such)
	\item The error estimation as of the above subsection
\end{enumerate}
>>>>>>> e271e0dd4e03f9b32630f2692f497c5ab7f1774a
