%!TEX root = ../report.tex
\section{Lagrangian interpolation}
To consider stability, we look at a polynomial through some set of exact values \(f(x_i)\), and a polynomial through a set of perturbed values \(\tilde{f}(x_i)\).
The maximum difference between those polynomials gives some measure of stability.
This measure of stability is bounded by Lebesgue's constant (\cite[Eq. 3.8]{quarteroni2014}):
\[
	\max_{x \in I}\abs{\Pi_n f(x) - \Pi_n \tilde{f}(x)}
	\leq
	\Lambda_n(\mathbf{x})\max_{0\leq i \leq n} \abs{f(x_i) - \tilde{f}(x_i)}.
\]
It can be seen that Lebesgue's constant is determined just by the nodes \(\mathbf{x}=\left\{x_i\right\}\).

An estimate for Lebesgue's constant for \(n + 1\) equidistant nodes is (\cite[Eq. 3.10]{quarteroni2014}):
\begin{equation}
	\label{eq:lebesgue_equidistant}
	\Lambda_n(\mathbf{x}) \simeq \frac{2^{n + 1}}{\euler n(\log n + \gamma)}.
\end{equation}

For Chebyshev-Gauss-Lobatto nodes, the Lebesgue number is approximated by 
\begin{equation}
	\label{eq:lebesgue_gauss_lobatto}
	\Lambda_n(\mathbf{x}) < \frac{2}{\pi}\left(\log n + \gamma + \log\frac{8}{\pi}\right) + \frac{\pi}{72n^2}
\end{equation}

In both \Autoref{eq:lebesgue_equidistant,eq:lebesgue_gauss_lobatto}, \(\gamma\) refers to the Euler constant (\(\approx 0.57721)\) and \(e\) is Euler's number (\(\approx 2.71828\)).
\subsection{Experiment}
Runge's function on the interval \(I = \lbrack-1, 1\rbrack\) is given by:
\[
	\frac{1}{1 + 25 x^2}.
\]
If one tries to interpolate Runge's function on \(I\) using equidistant nodes, the stability will be very low, as \autoref{fig:runges_problem} illustrates.
Between the nodes, the interpolation function greatly differs from the original function, which is undesirable.

\begin{figure}[htb]
	\centering
	\includegraphics[width=0.8\columnwidth]{images/runges_problem.eps}
	\caption{
		Illustration of Runge's problem.
		It can be seen that as \(n\) increases, the interpolation oscillates increasingly at the boundaries of the interval.
	}
	\label{fig:runges_problem}
\end{figure}

When using Chebyshev-Gauss-Lobatto nodes, the interpolation is more stable, as \autoref{fig:runges_solution} illustrates.

\begin{figure}[htb]
	\centering
	\includegraphics[width=0.8\columnwidth]{images/runges_solution.eps}
	\caption{
		Interpolation of Runge's function in \(\lbrack-1, 1\rbrack\) with Chebyshev-Gauss-Lobatto nodes.
		It can be seen that as the number of nodes increases, the accuracy increases up to the point that the lines are indistinguishable.
	}
	\label{fig:runges_solution}
\end{figure}

The experimental instability\footnote{We think stability is not a very appropriate term for this concept, since if the value is high it means that the interpolation is \emph{unstable}.} has been determined by evaluating the maximum absolute difference between the interpolating functions.
This has been done for both the equidistant and the Chebyshev-Gauss-Lobatto nodes.
The results can be seen in \autoref{fig:lebesque_plots}.

\begin{figure}[htb]
	\centering
	\includegraphics[width=0.48\columnwidth]{images/lebesque_equi.eps}
	~
	\includegraphics[width=0.48\columnwidth]{images/lebesque_cgl.eps}
	\caption{
		Lebesque's constants of equidistant and Chebyshev-Gauss-Lobatto nodes.
		Note that the left image has a logarithic \(y\)-axis, while the right image does not.
	}
	\label{fig:lebesque_plots}
\end{figure}

Apart from a constant factor and some occasional artifacts, the experimental results of the equidistant nodes nicely follow the theoretical estimates.
The same holds for the Chebyshev-Gauss-Lobatto nodes.

Not only the stability is improved when using Chebyshev-Gauss-Lobatto nodes, also the error is decreased.
This is illustrated in \autoref{fig:error_comparison_equi_cgl}, which shows the error of the two different types of nodes as a function of \(n\).

\begin{figure}[htb]
	\centering
	\includegraphics[width=0.7\columnwidth]{images/nodes_error.eps}
	\caption{
		Interpolation error of Runge's function with equidistant nodes (blue) and Chebyshev-Gauss-Lobatto nodes (red) as a function of \(n\).
	}
	\label{fig:error_comparison_equi_cgl}
\end{figure}

\clearpage
