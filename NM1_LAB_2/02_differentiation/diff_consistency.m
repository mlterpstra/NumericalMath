% INPUT
% f         either a function handle or a string containing the
%               name of a function
% jacobian   either a function handle or a string containing the
%               name of a function that evaluates the Jacobian
%               matrix at some point (a function of 'x')
% x         is an n x 1 array, the point at which to evaluate
%               the Jacobian
% v         the direction in which to compute the directional
%               derivative
% iMax      maximum number of refinements
% h0        initial h
% varargin  additional parameters for function evaluation
% OUTPUT
% diffNorm  array containing the norms of the errors
% hList     array containing values of h
function [diff_norm, hs] = diff_consistency(f, jacobian, x, v, i_max, h0, varargin)
    diff_norm = zeros(1, i_max);
    hs = zeros(1, i_max);
    for q = 1:i_max
        h = h0 * 10.^(-q);
        if h < eps
            diff_norm = diff_norm(1:q - 1);
            hs = hs(1:q - 1);
            return
        end
        % We scientists have found this one weird trick to apply a vector
        % as function arguments. The rest hates us!
        moved = num2cell(x + h*v);
        x_cell = num2cell(x);
        approx = (f(moved{:}) - f(x_cell{:})) / h;
        exact = jacobian(x)*v;
        
        diff_norm(q) = norm(approx - exact);
        hs(q) = h;
    end
end