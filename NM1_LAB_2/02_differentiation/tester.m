rng(10);
f = @(x,y) sin(x) + exp(y);
calc_jacob = @(x) [cos(x(1)), exp(x(2))];
n = 2;
x = rand(n, 1);
v = rand(n, 1);
v = v / norm(v);
i_max = 1e5;
h0 = 1;

[norms, hs] = diff_consistency(f, calc_jacob, x, v, i_max, h0);
min_h = hs(norms == min(norms));
disp(['The best h-value is ', num2str(min_h)]);
loglog(hs, norms, 'o-');
% possibly witha log scale for legibility
xlabel('h')
ylabel('difference')
figure;
plot(hs, norms, 'o-')