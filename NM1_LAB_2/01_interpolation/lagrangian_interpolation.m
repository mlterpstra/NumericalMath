% n-values to test
ns = 10:1:30;
num_ns = numel(ns);

% Runge's function
runge = @(x) 1./(1 + 25.*x.^2);

% Preallocate arrays for experimental and theoretical lambdas for both node
% types.
lambda_exp_equi = zeros(1, num_ns);
lambda_theo_equi = zeros(1, num_ns);
errs_equi = zeros(1, num_ns);

lambda_exp_cgl = zeros(1, num_ns);
lambda_theo_cgl = zeros(1, num_ns);
errs_cgl = zeros(1, num_ns);

for i=1:num_ns
    % Determine the stability for the runge function using equidistant
    % points and a maximum pertubation of 0.01.
    [err_equi, stab_equi] = lagrangeStability(runge, equidistant_nodes(ns(i)), 1e-2, false);
    lambda_exp_equi(i) = stab_equi;
    errs_equi(i) = err_equi;
    lambda_theo_equi(i) = lebesque('equidistant', ns(i));
    
    % Do the same for Chebyshev-Gauss-Lobatto nodes.
    [err_cgl, stab_cgl] = lagrangeStability(runge, cgl_nodes(ns(i)), 1e-2, false);
    lambda_exp_cgl(i) = stab_cgl;
    errs_cgl(i) = err_cgl;
    lambda_theo_cgl(i) = lebesque('cgl', ns(i));
end

figure;
title('Lebesque''s constants of equidistant nodes');
hold on;
plot(ns, lambda_exp_equi, 'o-');
plot(ns, lambda_theo_equi, 'o-');
set(gca,'yscale','log'); % logscale for readability
xlabel('n');
ylabel('\Lambda_n');
legend('Experimental', 'Theoretical', 'Location', 'SouthOutside');

figure;
title('Lebesque''s constants of Chebyshev-Gauss-Lobatto nodes');
hold on;
plot(ns, lambda_exp_cgl, 'o-');
plot(ns, lambda_theo_cgl, 'o-');
xlabel('n');
ylabel('\Lambda_n');
legend('Experimental', 'Theoretical', 'Location', 'SouthOutside');

figure;
title('Comparison of error');
hold on;
plot(ns, errs_equi, 'o-');
plot(ns, errs_cgl, 'o-');
set(gca,'yscale','log'); % logscale for readability
xlabel('n');
ylabel('E_n');
legend('Equidistant', 'Chebyshev-Gauss-Lobatto', 'Location', 'SouthOutside');
