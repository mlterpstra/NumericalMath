function [nodes] = equidistant_nodes(n, a, b)
    % EQUIDISTANT_NODES Returns an array of n equidistant nodes in the
    % interval [a, b].
    
    % Generate nodes in interval [-1, 1]
    nodes = -1 + (2 * ((0:n)) / n);
    
    % Scale these points to [a, b]
    if nargin > 1
        nodes = ((nodes + 1) / 2) .* (b - a) + a;
    end
end
