function [ lambda ] = lebesque(grid_type, n)
    % LEBESQUE Returns Lebesque's constant for the specified grid_type,
    % using n nodes.
    switch grid_type
        case 'equidistant'
            % Eq. (3.10)
            lambda = 2^(n + 1) / (exp(1) * n * (log(n) + eulergamma));
        case 'cgl'
            % Eq. (3.14)
            lambda = (2 / pi) * (log(n + 1) + eulergamma + log(8 / pi)) + pi / (72 * (n + 1)^2);
    end
    
    % Evaluate the result.
    lambda = vpa(lambda);
end
