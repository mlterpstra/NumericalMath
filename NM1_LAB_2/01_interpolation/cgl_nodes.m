function [nodes] = cgl_nodes(n, a, b)
    % CGL_NODES Returns an array of n Chebyshev-Gauss-Lobatto nodes in the
    % interval [a, b].
    
    % Generate nodes in interval [-1, 1]
    nodes = -cos(pi * ((0:n)) / n);
    
    % Scale these points to [a, b]
    if nargin > 1
        nodes = ((nodes + 1) / 2) .* (b - a) + a;
    end
end
