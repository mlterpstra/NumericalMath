epsilon = 1e-2;
runge = @(x) 1./(1 + 25.*x.^2);

figure;
subplot(2, 2, 1);
lagrangeStability(runge, equidistant_nodes(10), 1e-2, true);
ylim([-1, 2]);
title('n = 10');

subplot(2, 2, 2);
lagrangeStability(runge, equidistant_nodes(20), 1e-2, true);
ylim([-1, 2]);
title('n = 20');

subplot(2, 2, 3);
lagrangeStability(runge, equidistant_nodes(30), 1e-2, true);
ylim([-1, 2]);
title('n = 30');

hL  = legend('Original function', 'Interpolating polynomial',...
		    'Perturbed interpolating polynomial', 'Location', 'SouthOutside');
set(hL,'Position', [0.7 0.2 0.05 0.2],'Units', 'normalized');

figure;
subplot(2, 2, 1);
lagrangeStability(runge, cgl_nodes(10), 1e-2, true);
ylim([-1, 2]);
title('n = 10');

subplot(2, 2, 2);
lagrangeStability(runge, cgl_nodes(20), 1e-2, true);
ylim([-1, 2]);
title('n = 20');

subplot(2, 2, 3);
lagrangeStability(runge, cgl_nodes(30), 1e-2, true);
ylim([-1, 2]);
title('n = 30');

hL  = legend('Original function', 'Interpolating polynomial',...
		    'Perturbed interpolating polynomial', 'Location', 'SouthOutside');
set(hL,'Position', [0.7 0.2 0.05 0.2],'Units', 'normalized');