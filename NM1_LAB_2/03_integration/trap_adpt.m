% Adaptive simpson formula using error estimating by mesh halving
% INPUT
% f         the integrand, either a function handle or a string containing the
%               name of a function
% a         beginpoint of the integration interval
% b         endpoint of the integration interval
% tol       desired accuracy
% hMin      smallest permissable interval length
% varargin  optional parameters for function evaluation
%               (e.g. feval(f, x, varargin{:}))
% OUTPUT
% int       approximation to integral of f over the
%           interval [a,b]
% flag      if 1: tol not attained, 0: tol attained
% stats     struct containing data about error estimates per number of intervals
function [int, flag, stats] = trap_adpt(f, a, b, tol, hMin, varargin)

% f is redefined to ommit the usage of "varargin{:}" in any subsequent calls
f = @(x) feval(f, x, varargin{:});

% actIntervals is an array where each row corresponds to an interval and hence
% contains 5 nodes (each of these intervals is not yet sufficiently accurate)
actIntervals = linspace(a, b, 3);                                       % CHANGE

% Initialisation
% Function values (actFevals = feval(f, actIntervals) should always hold)
actFevals = f(actIntervals);
% Meshwidth of active interval (the coarse meshwidth)
h = b - a;
% The sum of the integral over all subintervals that are no longer active
int = 0;    
flag = 0;

% First row contains weights for integrating left half of interval
fineWeights = 1 / 2 * [1 1 0; 0 1 1].';                         
coarseWeights = 1 / 2 * [1 0 1].';                                 
% Estimate local integral
coarseEst = h * actFevals * coarseWeights;
fineEst = h / 2 * actFevals * fineWeights;
% Stats (do only when the third output is asked, i.e. if nargout == 3)
if (nargout == 3)
    stats = struct('totalErEst', 0, 'totalNrIntervals', 0, 'nodesList', actIntervals.',...
                   'totalGoodErEst', 0, 'totalNrGoodIntervals', 0);
end

% Continue untill there are no longer any active nodes (or h < hMin)
while (true)
    % estimate the error on all active subintervals
    [errorEst, goodIntervals] = estimateError(fineEst, coarseEst, tol * h / (b - a));
    if (nargout == 3)
        stats = updateStats(stats, errorEst, goodIntervals, actIntervals);
    end

    % Add integral of subintervals for which the local tolerance is attained
    int = int + sum(fineEst(goodIntervals, 1) + fineEst(goodIntervals, 2));
    if (all(goodIntervals))
        break; % Terminate the while loop
    end
    actIntervals = actIntervals(~goodIntervals, :);
    actFevals = actFevals(~goodIntervals, :);
    fineEst = fineEst(~goodIntervals, :);

    % Bisect the active intervals
    h = h / 2;
    if (h < hMin) % Check if h is not too small
        int = int + sum(fineEst(:, 1) + fineEst(:, 2)); % Use best approximation available
        flag = 1;
        break; % Terminate the while loop
    end

    [actIntervals, actFevals] = bisectIntervals(actIntervals, actFevals, f);

    if (nargout == 3) % Append new nodes
        stats.nodesList = [stats.nodesList; actIntervals(:, 2)];
    end
    [fineEst, coarseEst] = computeEstimates(fineEst, actFevals, fineWeights, h);
end

if (nargout == 3)
    if (stats.totalErEst(end) < tol && flag == 1)
        flag = 0;
    end
    stats.totalNrIntervals(1) = []; % Remove leading (initialisation) zeros
    stats.totalErEst(1) = [];
end
end

% Bisect the intervals while reusing the old function evaluations, and
% evaluate the function at the new nodes. Hence the number of rows of
% actIntervals and actFevals are doubled.
function [actIntervals, actFevals] = bisectIntervals(actIntervals, actFevals, f)
    % New nodes get (look at columns):
    %   1. Full weight of the original left node (hence: 1, 0)
    %   2. Average of the two original nodes (hence: 1 / 2, 1 / 2)
    %   3. Full weight of the original right node (hence: 0, 1)
    intervalBisector = [1, 1 / 2, 0;
                        0, 1 / 2, 1];
    
    % Replace every row (interval) in actIntervals with two finer rows
    % (intervals). Note: Multiple vector PRE-multiplications, where the
    % vectors are the original (coarse) intervals.
    actIntervals = [actIntervals(:, [1 2]) * intervalBisector;
                    actIntervals(:, [2 3]) * intervalBisector];
    
    % Add new elements for where the function has to be evaluated. This is
    % only one new value for every new interval.
    actFevals = [
        [actFevals(:, 1), zeros(size(actFevals(:, 1))), actFevals(:, 2)];
        [actFevals(:, 2), zeros(size(actFevals(:, 1))), actFevals(:, 3)]
    ];
    
    % Evaluate the function at the new nodes.
    actFevals(:, 2) = f(actIntervals(:, 2));
end

% Compute local estimates of the integral by: 1) reusing old fine estimates 2)
% using the new function evaluations.
function [fineEst, coarseEst] = computeEstimates(fineEst, actFevals, fineWeights, h)
    coarseEst = [fineEst(:, 1); fineEst(:, 2)];
    fineEst = h / 2 * actFevals * fineWeights;
end

% Estimate the error by mesh halving and determine good subintervals
% which are no longer active (logical index)
function [errorEst, goodIntervals] = estimateError(fineEst, coarseEst, relTol)
    % CHANGE! Note: I think we only need to change the constant here
    % (1 / 15). But I really do not see where it comes from. POORLY
    % DOCUMENTED CODE. Niet best. Ik zie nu dat het van p. 129 komt. Maar
    % dan nog. Geen idee.
    % zie http://www.sci.brooklyn.cuny.edu/~mate/nml/numanal.pdf, pagina 76-77
    % Dit lijkt te werken, vereist wel een kleinere tolerance dan simpson 
    % voor zelfde resultaat maar dat is te verwachten
    errorEst = 1 / 3 * (coarseEst - fineEst(:, 1) - fineEst(:, 2));
    goodIntervals = (abs(errorEst) < relTol);
end

function [stats] = updateStats(stats, errorEst, goodIntervals, actIntervals)
    badErEst = sum(abs(errorEst(~goodIntervals)));
    stats.totalGoodErEst = stats.totalGoodErEst + sum(abs(errorEst(goodIntervals)));
    stats.totalNrGoodIntervals = stats.totalNrGoodIntervals + sum(goodIntervals);

    % update the total error estimate (good and bad together)
    stats.totalErEst = [stats.totalErEst; stats.totalGoodErEst + badErEst];

    % update the total number of intervals
    stats.totalNrIntervals = [stats.totalNrIntervals;
                        stats.totalNrGoodIntervals + size(actIntervals(~goodIntervals), 1)];
end
