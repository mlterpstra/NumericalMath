% Composite trapezoidal formula using error estimating
% by mesh halving
% INPUT
% f         string with name of function
% a         beginpoint of the integration interval
% b         endpoint of the integration interval
% tol       minimum desired error
% hMin      smallest permissable interval length
% varargin  parameters for function evaluation
% OUTPUT
% int       approximation to integral of f over the
%           interval [a,b]
% flag      if 1: tol not attained, 0: tol attained
% stats     struct containing data about error estimates per number of intervals
function [int, flag, stats] = trap_comp(f, a, b, tol, hMin, varargin)

% Initialise variables
h = b - a;
int = h / 2 * (feval(f, a, varargin{:}) + feval(f, b, varargin{:}));
flag = 1;

if (nargout == 3)
    stats = struct('totalErEst', [], 'totalNrIntervals', [], 'nodesList', []);
end

% Nr of times mesh is halved - 1
i = 1;
while (h > hMin)
    h = h / 2;
    if (h < eps) % Check if h is not "zero"
        break;
    end

    % New nodes
    newNodes = a + (b - a) * [1 : 2 : (2 ^ i - 1)] / (2 ^ i);
    % Update integral
    intNew = int / 2 + h * sum(feval(f, newNodes, varargin{:}));

    % Estimate the error
    errorEst = 1 / 3 * (int - intNew);
    int = intNew;
    if (nargout == 3) % Update stats
        stats.totalErEst = [stats.totalErEst; abs(errorEst)];
        stats.totalNrIntervals = [stats.totalNrIntervals; 2 ^ (i - 1)];
    end

    if (abs(errorEst) < tol)
        flag = 0;
        break
    end
    i = i + 1;
end

end
