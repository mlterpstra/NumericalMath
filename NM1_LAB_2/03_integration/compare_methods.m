close all;
clear all;

f = @(x) atan(sqrt(x));
a = 0;
b = 2;

exact_integral = 3 * atan(sqrt(2)) - sqrt(2);

tols = 10.^-(1:12);

avg_over = 10;

approx_trap_comp = zeros(1, numel(tols));
approx_trap_adpt = zeros(1, numel(tols));
approx_simp_adpt = zeros(1, numel(tols));
n_intervals_trap_comp = zeros(1, numel(tols));
n_intervals_trap_adpt = zeros(1, numel(tols));
n_intervals_simp_adpt = zeros(1, numel(tols));
t_trap_comp = zeros(1, numel(tols));
t_trap_adpt = zeros(1, numel(tols));
t_simp_adpt = zeros(1, numel(tols));
error_estimate_trap_comp = zeros(1, numel(tols));
error_estimate_trap_adpt = zeros(1, numel(tols));
error_estimate_simp_adpt = zeros(1, numel(tols));
% stats_trap_comp = zeros(1, numel(tols));
% stats_trap_adpt = zeros(1, numel(tols));
% stats_simp_adpt = zeros(1, numel(tols));

% feature accel off;
for idx=1:numel(tols)
    tol = tols(idx);

    tic;
    for repeat=1:avg_over
    	[approx_trap_adpt(idx), ~, stats] = trap_adpt(f, a, b, tol, eps);
    end
    t_trap_adpt(idx) = toc / avg_over;
    n_intervals_trap_adpt(idx) = stats.totalNrIntervals(end);
    error_estimate_trap_adpt(idx) = stats.totalErEst(end);
    stats_trap_adpt(idx) = stats;

    tic;
    for repeat=1:avg_over
	    [approx_trap_comp(idx), ~, stats] = trap_comp(f, a, b, tol, eps);
    end
    t_trap_comp(idx) = toc / avg_over;
    n_intervals_trap_comp(idx) = stats.totalNrIntervals(end);
    error_estimate_trap_comp(idx) = stats.totalErEst(end);
    stats_trap_comp(idx) = stats;

    tic;
    for repeat=1:avg_over
    	[approx_simp_adpt(idx), ~, stats] = simp_adpt(f, a, b, tol, eps);
    end
    t_simp_adpt(idx) = toc / avg_over;
    n_intervals_simp_adpt(idx) = stats.totalNrIntervals(end);
    error_estimate_simp_adpt(idx) = stats.totalErEst(end);
    stats_simp_adpt(idx) = stats;
end

figure;
loglog(n_intervals_trap_comp, error_estimate_trap_comp);
hold on;
loglog(n_intervals_trap_adpt, error_estimate_trap_adpt);
loglog(n_intervals_simp_adpt, error_estimate_simp_adpt);
xlabel('# intervals');
ylabel('Error estimate');
legend('Composite trapezoidal', 'Adaptive trapezoidal', 'Adaptive Simpson');

figure;
loglog(tols, t_trap_comp);
hold on;
loglog(tols, t_trap_adpt);
loglog(tols, t_simp_adpt);
xlabel('Tolerance');
ylabel('Computing time');
legend('Composite trapezoidal', 'Adaptive trapezoidal', 'Adaptive Simpson');

% Find the point where adaptive becomes more efficient than composite.
point = find(t_trap_adpt - t_trap_comp < 0, 1);

figure;
loglog(stats_trap_comp(point).totalErEst, stats_trap_comp(point).totalNrIntervals);
hold on;
loglog(stats_trap_adpt(point).totalErEst, stats_trap_adpt(point).totalNrIntervals);
title(['tol = ', num2str(tols(point))]);
xlabel('Error estimate');
ylabel('# intervals');
legend('Composite trapezoidal', 'Adaptive trapezoidal');
