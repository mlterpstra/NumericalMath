% Adaptive simpson formula using error estimating by mesh halving
% INPUT
% f         the integrand, either a function handle or a string containing the
%               name of a function
% a         beginpoint of the integration interval
% b         endpoint of the integration interval
% tol       desired accuracy
% hMin      smallest permissable interval length
% varargin  optional parameters for function evaluation
%               (e.g. feval(f, x, varargin{:}))
% OUTPUT
% int       approximation to integral of f over the
%           interval [a,b]
% flag      if 1: tol not attained, 0: tol attained
% stats     struct containing data about error estimates per number of intervals
function [int, flag, stats] = simp_adpt(f, a, b, tol, hMin, varargin)

% f is redefined to ommit the usage of "varargin{:}" in any subsequent calls
f = @(x) feval(f, x, varargin{:});

% actIntervals is an array where each row corresponds to an interval and hence
% contains 5 nodes (each of these intervals is not yet sufficiently accurate)
actIntervals = linspace(a, b, 5);                                       % CHANGE

% Initialisation
% Function values (actFevals = feval(f, actIntervals) should always hold)
actFevals = f(actIntervals);
% Meshwidth of active interval (the coarse meshwidth)
h = b - a;
% The sum of the integral over all subintervals that are no longer active
int = 0;
flag = 0;

% First row contains weights for integrating left half of interval
fineWeights = 1 / 6 * [1 4 1 0 0; 0 0 1 4 1].';                         % CHANGE
coarseWeights = 1 / 6 * [1 0 4 0 1].';                                  % CHANGE
% Estimate local integral
coarseEst = h * actFevals * coarseWeights;
fineEst = h / 2 * actFevals * fineWeights;
% Stats (do only when the third output is asked, i.e. if nargout == 3)
if (nargout == 3)
    stats = struct('totalErEst', 0, 'totalNrIntervals', 0, 'nodesList', actIntervals.',...
                   'totalGoodErEst', 0, 'totalNrGoodIntervals', 0);
end

% Continue untill there are no longer any active nodes (or h < hMin)
while (true)
    % estimate the error on all active subintervals
    % Note: h / (b - a) is 1 in first iteration, and becomes smaller,
    % because smaller intervals are allowed to make a smaller error.
    % Note: coarseEst is an array that represents the integral at one level
    % coarser that fineEst. Contains only those parts that still need to be
    % evaluated finer.
    [errorEst, goodIntervals] = estimateError(fineEst, coarseEst, tol * h / (b - a)); %
    if (nargout == 3)
        stats = updateStats(stats, errorEst, goodIntervals, actIntervals);
    end
    
    % Add integral of subintervals for which the local tolerance is attained
    int = int + sum(fineEst(goodIntervals, 1) + fineEst(goodIntervals, 2));
    if (all(goodIntervals))
        break; % Terminate the while loop
    end
    actIntervals = actIntervals(~goodIntervals, :);
    actFevals = actFevals(~goodIntervals, :);
    fineEst = fineEst(~goodIntervals, :);

    % Bisect the active intervals
    h = h / 2;
    if (h < hMin) % Check if h is not too small
        int = int + sum(fineEst(:, 1) + fineEst(:, 2)); % Use best approximation available
        flag = 1;
        break; % Terminate the while loop
    end
    
    [actIntervals, actFevals] = bisectIntervals(actIntervals, actFevals, f);

    if (nargout == 3) % Append new nodes
        stats.nodesList = [stats.nodesList; actIntervals(:, 2);         % CHANGE
                           actIntervals(:, 4)];                         % CHANGE
    end
    [fineEst, coarseEst] = computeEstimates(fineEst, actFevals, fineWeights, h);
end

if (nargout == 3)
    if (stats.totalErEst(end) < tol && flag == 1)
        flag = 0;
    end
    stats.totalNrIntervals(1) = []; % Remove leading (initialisation) zeros
    stats.totalErEst(1) = [];
end
end

% Bisect the intervals while reusing the old function evaluations, and
% evaluate the function at the new nodes. Hence the number of rows of
% actIntervals and actFevals are doubled.
function [actIntervals, actFevals] = bisectIntervals(actIntervals, actFevals, f)
    intervalBisector = [1, 1 / 2, 0, 0    , 0;                          % CHANGE
                        0, 1 / 2, 1, 1 / 2, 0;                          % CHANGE
                        0, 0    , 0, 1 / 2, 1];                         % CHANGE

    actIntervals = [actIntervals(:, [1 2 3]) * intervalBisector;        % CHANGE
                    actIntervals(:, [3 4 5]) * intervalBisector];       % CHANGE

    actFevals = [[actFevals(:, 1), zeros(size(actFevals(:, 1))),...     % CHANGE
                  actFevals(:, 2), zeros(size(actFevals(:, 1))),...     % CHANGE
                  actFevals(:, 3)];                                     % CHANGE
                 [actFevals(:, 3), zeros(size(actFevals(:, 1))),...     % CHANGE
                  actFevals(:, 4), zeros(size(actFevals(:, 1))),...     % CHANGE
                  actFevals(:, 5)]];                                    % CHANGE

    actFevals(:, [2, 4]) = f(actIntervals(:, [2, 4]));                  % CHANGE
end

% Compute local estimates of the integral by: 1) reusing old fine estimates 2)
% using the new function evaluations.
function [fineEst, coarseEst] = computeEstimates(fineEst, actFevals, fineWeights, h)
    coarseEst = [fineEst(:, 1); fineEst(:, 2)];
    fineEst = h / 2 * actFevals * fineWeights;
end

% Estimate the error by mesh halving and determine good subintervals
% which are no longer active (logical index)
function [errorEst, goodIntervals] = estimateError(fineEst, coarseEst, relTol)
    errorEst = 1 / 15 * (coarseEst - (fineEst(:, 1) + fineEst(:, 2)));    % CHANGE
    goodIntervals = (abs(errorEst) < relTol);
end

function [stats] = updateStats(stats, errorEst, goodIntervals, actIntervals)
    badErEst = sum(abs(errorEst(~goodIntervals)));
    stats.totalGoodErEst = stats.totalGoodErEst + sum(abs(errorEst(goodIntervals)));
    stats.totalNrGoodIntervals = stats.totalNrGoodIntervals + sum(goodIntervals);

    % update the total error estimate (good and bad together)
    stats.totalErEst = [stats.totalErEst; stats.totalGoodErEst + badErEst];

    % update the total number of intervals
    stats.totalNrIntervals = [stats.totalNrIntervals;
                        stats.totalNrGoodIntervals + size(actIntervals(~goodIntervals), 1)];
end
