close all; clear all;

bar_length = 1; % Lenght of the bar
hs_length = 0.2; % Length of the heat source
hs_cross = 1^2; % Cross section of the bar
hs_power = 2e7; % Power of the heat source in W/m^3
rho = 7.874e3; % Density in kg/m^3
k = 8.04e1; % Thermal conductivity in W/(K m)
c_p = 4.5e2; % Specific heat capacity in J / (kg K)

% This is trivial physics, and is assumed common knowledge. Reference for
% unskillful people: https://en.wikipedia.org/wiki/Thermal_diffusivity
mu = k / (rho * c_p);

T_0 = 293; % Initial temperature in K
T_melt = 1811; % Melting temperature of iron in K

% boundary conditions
g = @(x) repmat(T_0, size(x));

% p is the step function
p = @(x, t) (hs_power / (rho * c_p * hs_cross * hs_length)) * double(x > 0.4 & x < 0.6);
init_v = @(x) repmat(T_0, size(x));

N = 100;
h = 1/N;
dt = 0.05;
t_end = 60;
theta = 1;

[ts, sol_array, nodes] = heatSolveTheta(p, init_v, mu, theta, t_end, N, dt, g);
surf(nodes, ts, sol_array, 'EdgeColor','none','LineStyle','none','FaceLighting','phong');
xlabel('x');
ylabel('t');
zlabel('T');