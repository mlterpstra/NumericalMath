% Solves the 1D Poisson problem 
%       -laplace u = f on (0,1)
% with Dirichlet boundary conditions given by g
% INPUT
% f         right-hand side function
% g         Dirichlet boundary condition function
% N         number of subintervals
% method    'bs', none', 'jacobi' or 'gs'
% tol       tolerance for iterative solver
% OUTPUT
% sol       (N+1) x 1 solution array
% nodes     (N+1) x 1 array with location of spatial nodes
function [sol, nodes] = poissonSolveFD(f, g, N, method, tol)
	nodes = linspace(0, 1, N + 1)';
	h = 1 / N;
	b = f(nodes(2:end - 1)) .* (h*h);
	b(1) = b(1) + g(nodes(1));
	b(end) = b(end) + g(nodes(end));
	A = makeLaplace(N);

	if strcmp(method, 'bs')
		sol = A \ b;
	else
		if strcmp(method, 'none')
			P = [];
		elseif strcmp(method, 'jacobi')
			P = diag(diag(A));
		elseif strcmp(method, 'gs');
			P = tril(A, 0);
		end
		maxIt = 10 * numel(b);
		[sol, flag, ~] = iterMethod(A, b, zeros(size(b)), tol, maxIt, P, 2, 0);
		if flag
			warning('iterMethod did not converge.');
		end
	end

	% Concatenate boundary condition to endpoints
	sol = [g(nodes(1)); sol; g(nodes(end))];
end


% Attempts to solve A * x = b, with initial guess x0 using 
% an iterative method of the form
%           x^{k+1} = x^{k} + alpha_k P \ r^{k}, r^{k} = b - A x^{k}
% where alpha_k = alpha0 if dynamic = 0
% INPUT
% A         n x n matrix
% b         n x 1 right-hand side
% x0        initial guess
% tol       desired tolerance
% maxIt     maximum number of iterations
% P         preconditioner of A (optional)
% dynamic   0: static, 1: minimise A-norm of error, 2: minimise
%           2-norm of residual
% alpha0    if dynamic = 0, this value is used for alpha_k
% OUTPUT
% x         approximate solution to A * x = b
% flag      if 0 then tolerance is attained
% convHist  relative residual per iteration
function [x, flag, convHist] = iterMethod(A, b, x0, tol, maxIt,...
    P, dynamic, alpha0)
	it = 0;
	x_k = x0;
	cont = true;
	convHist = zeros(1, maxIt);
	% Dynamically test if a matrix or function handle is passed
	% if a matrix is passed, evaluate the matrix/vector product A * v
	% otherwise evaluate the function handle which somehow computes Av efficiently
	if isa(A, 'function_handle')
		mat_vec = A;
	else
		mat_vec = @(x) A * x;
	end
	while cont && it < maxIt
		% The matrix vector product is only computed once per iteration
		r_k = b - mat_vec(x_k);
		% If no preconditioner is given, use only the residual
		if ~isempty(P)
			z_k = P \ r_k;
		else
			z_k = r_k;
		end
		switch(dynamic)
			case 0
				alpha_k = alpha0;
			case 1
				alpha_k = (z_k' * r_k) / (z_k' * (mat_vec(z_k)));
			case 2
				alpha_k = ((mat_vec(z_k))' * r_k) / ((mat_vec(z_k))' * (mat_vec(z_k)));
			otherwise
				error(disp('Invalid value for dynamic, exiting...'));
		end

		x_k = x_k + alpha_k * z_k;
		cont = norm(r_k) > tol * norm(b);
		it = it + 1;
		convHist(it) = norm(r_k);
	end
	convHist = convHist(:, 1:it);
	x = x_k;
	flag = it == maxIt;
end