% Performs integration of the system of ODEs given by
%           d/dt u = f(t, u(t)), u(tRange(1)) = u0
% using the theta-method
% INPUT
% f         the right-hand side function, the output should be a
%           N x 1 array where N is the number of unknowns
% tRange    the time interval of integration
% u0        initial solution at t = tRange(1) (N x 1 array)
% df        a function that evaluates the jacobian of f
% theta     defines the method
% h         the step-size
% OUTPUT
% tArray    array containing the time points
% solArray  array containing the solution at each time level
%           (the ith row equals the solution at time tArray(i))
function [tArray, solArray] = odeSolveTheta(f, tRange, u0, df, theta, h)
	tArray = linspace(tRange(1), tRange(2), (tRange(2) - tRange(1)) / h + 1)';
	
	n_t = numel(tArray);
	n = numel(u0);

	solArray = zeros(n_t, n);
	solArray(1, :) = u0;

	if theta == 0
		for i = 2:n_t
			u_prev = solArray(i - 1, :);
			t_prev = tArray(i - 1);
			sol = u_prev(:) + h * f(t_prev, u_prev(:));
			solArray(i, :) = sol(:);
		end
	else
		for i = 2:n_t
			t_prev = tArray(i - 1);
			t_next = tArray(i);
			u_prev = solArray(i - 1, :);
			F = @(u) u_prev(:) + h * (theta * f(t_next, u(:)) + (1 - theta) * f(t_prev, u_prev(:))) - u(:); 
			J_F = @(u) theta * h * df(t_next, u(:)) - eye(n);
			[u_next, flag, ~, ~] = newton(F, J_F, u_prev, 1e-7, 1e3);
			if flag
				warning('Newton did not converge!');
			end
			solArray(i, :) = u_next;
		end
	end
end

% INPUT
% f         function of rootfinding problem
% df        function returning the Jacobian
% x_0        initial guess
% tol       desired tolerance
% maxIt     maximum number of iterations
% OUTPUT    
% root      if succesful, root is an approximation to the
%           root of the nonlinear equation
% flag      flag = 0: tolerance attained, flag = 1: reached maxIt
% iter      the number of iterations
% convHist  convergence history
function [root, flag, iter, convHist] = newton(f, df, x_0, tol, maxIt)
	x = reshape(x_0, numel(x_0), 1);
	convHist = zeros(maxIt, 1);

	flag = 1;
	for iter = 1 : maxIt
		x_next = phi(x, f, df);
		convHist(iter) = norm(x_next - x);
		x = x_next;

		if convHist(iter) < tol
			flag = 0;
			break;
		end
	end

	root = reshape(x, size(x_0));
	convHist(iter + 1 : end) = [];
end

function x_next = phi(x, f, df)
	x_next = x - (df(x) \ f(x));
end