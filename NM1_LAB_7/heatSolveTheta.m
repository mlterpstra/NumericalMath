% Solves the 1D heat eqn 
%       du/dt = mu laplace u + p(x,t) on (0,1) x (0, tEnd)
% and initial condition u0 (the Dirichlet boundary 
% conditions are imposed by u0)
% INPUT
% p         right-hand side forcing term (function of x and t)
% u0Func    initial condition function (function of x)
% jac 		a function handle that evaluates the jacobian of p/mu
% mu        diffusion coefficient
% theta     parameter for time integration
% tEnd      end time
% N         number of subintervals
% dt        step-size
% g 		Boundary condition
% OUTPUT
% tArray    array containing the time points
% solArray  array containing the solution at each time level
%           (the ith row equals the solution at time tArray(i))
%           (nrTimeSteps + 1) x (N+1) array
% nodes     (N+1) x 1 array with location of spatial nodes
function [tArray, solArray, nodes] = heatSolveTheta(p, u0Func , mu, theta, tEnd, N, dt, g)
	nodes = linspace(0, 1, N + 1)';
	internal_nodes = nodes(2:end - 1);
	h = 1 / N;
	b = p(internal_nodes, 0);
	b(1) = b(1) + mu * g(nodes(1)) / (h*h);
	b(end) = b(end) + mu * g(nodes(end)) / (h*h);
	A = makeLaplace(N);

	f = @(t, v) -(mu / h^2) * A * v + b;
	jac = @(t, v) -(mu / h^2) * A;

	tRange = [0, tEnd];
	[tArray, solArray] = odeSolveTheta_pcode(f, tRange, u0Func(internal_nodes), jac, theta, dt);
	solArray = [repmat(g(nodes(1)), numel(tArray), 1), solArray, repmat(g(nodes(end)), numel(tArray), 1)];
end