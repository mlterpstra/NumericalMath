%!TEX root = ../../report.tex
\subsection{The heat equation}
\subsubsection{Study stuff}
OK.

\subsubsection{Initial Boundary Value Problem}
The IBVP problem is given as follows:
\begin{align}
	\partial_t v &= \mu \laplace v + p \quad \text{for } (x, t) \in (0, 1) \times [0, T],\nonumber\\
	v(x, t) &= g(x) \quad \text{for } (x, t) \in \left\{0, 1\right\} \times [0, T], \label{eq:ibvp}\\
	v(x, 0) &= v_0(x).\nonumber
\end{align}
We use \(p(x, t) = \gamma \sin(\pi x)\) and \(g = 0\), with \(\gamma = \pi^2 \mu\).

We will show that \(\tilde{v}(x, t) = \left(1 + \euler^{-\gamma t}\right)\sin(\pi x)\) is a solution.

Writing out the LHS of the first equality in \autoref{eq:ibvp}, we get
\begin{align*}
	\partial_t \tilde{v} &= \partial_t \left(1 + \euler^{-\gamma t}\right)\sin(\pi x) \\
	&= \partial_t \left(1 + \euler^{-\gamma t}\right)\sin(\pi x) \\
	&= -\gamma\euler^{-\gamma t}\sin(\pi x).
\end{align*}
Writing out the RHS, we get
\begin{align*}
	\mu \laplace \tilde{v} + p &= \mu \laplace \left(1 + \euler^{-\gamma t}\right)\sin(\pi x) + \gamma \sin(\pi x) \\
	&= \mu \partial_x \partial_x \left(1 + \euler^{-\gamma t}\right)\sin(\pi x) + \gamma \sin(\pi x) \\
	&= \pi \mu \partial_x \left(1 + \euler^{-\gamma t}\right)\cos(\pi x) + \gamma \sin(\pi x) \\
	&= -\pi^2\mu \left(1 + \euler^{-\gamma t}\right)\sin(\pi x) + \gamma \sin(\pi x) \\
	&= -\gamma \left(1 + \euler^{-\gamma t}\right)\sin(\pi x) + \gamma \sin(\pi x) \\
	&= -\gamma \euler^{-\gamma t}\sin(\pi x).
\end{align*}

For the second equality, we can easily see that
\begin{align*}
	\tilde{v}(0, t) = \tilde{v}(1, t) = 0 = g(x).
\end{align*}

The third equality is trivial to show and is left as an exercise for the reader.

\subsubsection{Discretizing space}
Discretizing \autoref{eq:ibvp} in space makes use of the discretization of the Laplace operator.
We have already established that \(\laplace v\) is discretized as \(-\frac{1}{h^2}\mat{A}{v}\).

Hence it should come as no surprise that discretizing \autoref{eq:ibvp} in space in full results in
\begin{align*}
	\frac{\d\vec{v}}{\d t}(t) &= -\frac{\mu}{h^2}\mat{A}\vec{v}(t) + \vec{b}(t),\\
	\text{with} \quad &\vec{b}(t) = \begin{pmatrix}
		p_1(t) + \frac{\mu g_0}{h^2} \\
		p_2(t) \\
		p_3(t) \\
		\vdots \\
		p_{N-1}(t) + \frac{\mu g_N}{h^2}
	\end{pmatrix}
\end{align*}

\subsubsection{The \texorpdfstring{\(\theta\)}{theta}-method}
Using the \(\theta\)-method to solve the system of ODEs, we write out
\begin{align*}
	\vec{v}_{n+1} &= \vec{v}_n + \Delta t\left(\theta(-\frac{\mu}{h^2}\mat{A}\vec{v}_{n+1} + \vec{b}_{n+1}) + (1 - \theta)(-\frac{\mu}{h^2}\mat{A}\vec{v}_n + \vec{b}_n)\right) \\
	&= \vec{v}_n - \frac{\mu \Delta t}{h^2} (1 - \theta) \mat{A}\vec{v}_n - \frac{\mu \Delta t}{h^2} \theta \mat{A}\vec{v}_{n+1} + \Delta t \theta \vec{b}_{n+1} + \Delta t (1 - \theta) \vec{b}_n. \\
\end{align*}
Reordering the equation by bringing all \(\vec{v}_{n+1}\) terms to the LHS, we acquire
\begin{align*}
	\left(\mat{I} + \frac{\mu \Delta t}{h^2} \theta \mat{A}\right)\vec{v}_{n+1} &= \left(\mat{I} - \frac{\mu \Delta t}{h^2} (1 - \theta) \mat{A}\right) \vec{v}_n + \Delta t \left( \theta \vec{b}_{n+1} + (1 - \theta) \vec{b}_n \right).
\end{align*}

\subsubsection{Absolute stability}
According to \cite[Ex. 9.2]{quarteroni2014}, the eigenvalues of \(\mat{A}\) are given by
\begin{align}
	\label{eq:eigs}
	\lambda_k = 4\sin^2\left(\frac{k \pi}{2 N}\right) \quad \text{for } k = 1, \ldots N - 1.
\end{align}
We observe that \(\lambda_k \leq 4\).

For the explicit (forward) Euler method (\(\theta = 0\)) we have
\begin{align*}
	\vec{v}_{n+1} &= \left(\mat{I} - \frac{\mu \Delta t}{h^2} \mat{A}\right) \vec{v}_n + \Delta t \vec{b}_n \\
	&= \vec{v}_n + \Delta t \left(-\frac{\mu}{h^2} \mat{A} \vec{v}_n + \vec{b}_n\right).
\end{align*}
Explicit Euler is in the ROAS if  \(\abs{1 + \mu_k \Delta t} < 1\), where \(\mu_k\) are the eigenvalues of \(-\frac{\mu}{h^2} \mat{A}\).
So \(\mu_k \Delta t > -2\) is a condition for absolute stability.

\(\mu_k\) are bounded as follows:
\begin{align*}
	\mu_k = -\frac{\mu}{h^2}\lambda_k \geq -\frac{4 \mu}{h^2}.
\end{align*}
In other words, if \(-\frac{4 \mu}{h^2} \Delta t > -2\), then surely \(\mu_k \Delta t > -2\).
So we can guarantee stability if
\begin{align*}
	-\frac{4 \mu}{h^2} \Delta t &> -2 \\
	&\Leftrightarrow \\
	\frac{\mu \Delta t}{h^2} &< \frac{1}{2}.
\end{align*}