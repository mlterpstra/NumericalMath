% INPUT
% N         number of subintervals
% OUTPUT
% L         discrete Laplace operator (3-point stencil)
function L = makeLaplace(N)
	if N < 2^20
		% Very fast, but higher memory usage
		A = zeros(N - 1, 3);
		A(1:end - 1, 1) = -1;
		A( :       , 2) =  2;
		A(2:end    , 3) = -1;
		L = spdiags(A, [-1, 0, 1], N - 1, N - 1);
	else
		% Very slow but lower memory usage 
		L = 2 * speye(N - 1);

		for i = 1:N-2
			L(i + 1, i) = -1;
			L(i, i + 1) = -1;
		end
	end
end