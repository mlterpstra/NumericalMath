close all;
clear all;

mu = 1e-3;
gamma = pi * pi * mu;
u_tilde = @(x) sin(pi * x);
p = @(x, t) gamma * u_tilde(x);
g = @(x) 0;
tEnd = 1000;
v_dakje = @(x, t) (1 + exp(-gamma * t)) * u_tilde(x);
vi0 = @(x) v_dakje(x, 0);
N = 20;
h = 1/N;

dts = [0.625, 1.25, 1.35, 100, 200, 100, 200];
thetas = [0, 0, 0, 0.5, 0.5, 1, 1];

muDeltaTOverHSquared = zeros(1, numel(dts));
eMaxT = zeros(1, numel(dts));

% Array of axes handles
hax = zeros(1, numel(dts));

figure;
for i=1:numel(dts)
	dt = dts(i);
	theta = thetas(i);
	[tArray, solArray, nodes] = heatSolveTheta(p, vi0, mu, theta, tEnd, N, dt, g);
	% surf(tArray, solArray, nodes);
	exact = v_dakje(nodes, tEnd)';
	muDeltaTOverHSquared(i) = (mu * dt) / (h * h);
	eMaxT(i) = max(abs(solArray(end, :) - exact));
	hax(i) = subplot(2, 4, i);
	surf(nodes, tArray, solArray, 'EdgeColor','none','LineStyle','none','FaceLighting','phong');
	xlabel('x');
	ylabel('t');
	zlabel('v');
	title({sprintf('\\theta = %2.1f    dt = %7.3f', thetas(i), dts(i)), sprintf('\\mu\\Deltat/h^2 = %7.3f    e_{max} = %7.3e', muDeltaTOverHSquared(i), eMaxT(i))})
end

% Link the camera position in the handles of the multiple axes
% (Breaks the third plot, but that one is bullocks anyway.)
linkprop(hax, 'CameraPosition');

format compact;
fprintf('+---------+---------+---------+-----------+\n')
fprintf('|  theta  |    dt   |   frac  |  e_max(t) |\n')
fprintf('+---------+---------+---------+-----------+\n')

for i=1:numel(dts)
	
	fprintf('|   %2.1f   | %7.3f | %7.3f | %7.3e |\n', ...
		thetas(i), dts(i), muDeltaTOverHSquared(i), eMaxT(i));
end

fprintf('+---------+---------+---------+-----------+\n')
