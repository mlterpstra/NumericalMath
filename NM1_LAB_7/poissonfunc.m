close all; clear all;

Ns = 2.^(2:13);
errors = zeros(1, numel(Ns));
f = @(x) pi^2*sin(pi*x);
u_exact = @(x) sin(pi * x);
g = @(x) 0;

figure;
for i=1:numel(Ns)
	[sol, nodes] = poissonSolveFD(f, g, Ns(i), 'bs', 1e-10);
	plot(nodes, sol);
	hold on;
	exact = u_exact(nodes);
	errors(i) = max(abs(sol - exact));
end
title('Solutions for different values of h');
xlabel('x');
ylabel('u')

% Confirming Proposition 9.1
Cs = zeros(1, numel(Ns));
M = pi^4;
for i=1:numel(Ns)
	h = 1 / Ns(i);
	Cs(i) = errors(i) / (M * h^2);
end


hs = 1 ./ Ns;
figure;
loglog(hs, errors);
hold on;
loglog(hs, hs.^2);
title('Error confirmed to be order 2');
xlabel('$h$', 'Interpreter','LaTex')
ylabel('Error');
l_1 = legend('Errors', '$y = h^2$');
set(l_1, 'Interpreter', 'latex');

figure;
semilogx(hs, Cs);
title('C needed for equality in Prop. 9.1');
xlabel('$h$', 'Interpreter', 'LaTex')
ylabel('C', 'Interpreter', 'LaTex');