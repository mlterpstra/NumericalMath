close all; clear all;

N = 10;
A = gallery('poisson', N);
b = ones(N^2, 1) / (N^2);
experiments = iterCompare(A, b);
dynamic_gss_1 = [];
dynamic_gss_2 = [];
static_no_precond = [];
static_jacobi = [];
% Final element is conjugate gradient descent, so discard that one
for i=1:numel(experiments)-1
    experiment = experiments{i};
    if experiment.pType == 2
        if experiment.aType == 1
            dynamic_gss_1 = [dynamic_gss_1, experiment];
        end
        if experiment.aType == 2
            dynamic_gss_2 = [dynamic_gss_2, experiment];
        end
    end
    if experiment.aType == 0
        if experiment.pType == 0
            static_no_precond = [static_no_precond, experiment];
        end
        if experiment.pType == 1
            static_jacobi = [static_jacobi, experiment];
        end
    end
end

figure;
semilogy(1:numel(dynamic_gss_1(1).conv), dynamic_gss_1(1).conv)
hold on;
semilogy(1:numel(dynamic_gss_2(1).conv), dynamic_gss_2(1).conv)
title('Dynamic Gauss-Seidel');
xlabel('k');
ylabel('r^{(k)}');
legend('\alpha_k^1', '\alpha_k^2');

figure;
semilogy(1:numel(static_no_precond(1).conv), static_no_precond(1).conv)
hold on;
semilogy(1:numel(static_jacobi(1).conv), static_jacobi(1).conv)
title('Static \alpha');
xlabel('k');
ylabel('r^{(k)}');
legend('No preconditioner', 'Jacobi');

% Checking if k_min agrees with actual number of iterations
tol = 1e-12;
B_alpha = diag(diag(A)) \ (diag(diag(A)) - A);
x_0 = rand(N*N, 1);
[lambda, ~, ~, ~, ~, ~] = powerMethod(B_alpha, x_0, tol, 10*N^2);
spectral_radius = abs(lambda);

k_min = floor(log(tol * norm(b) / norm(A * x_0 - b)) / log(spectral_radius));

fprintf('Expected static Jacobi iterations: %d\nActual static Jacobi iterations: %d\n', k_min, numel(static_jacobi(1).conv));

figure;
surf(reshape(static_jacobi(1).x, N, N)), colormap(gray(50));