% Compare several iterative methods on input matrix A and rhs b
function [experiment] = iterCompare(A, b)

% Column 1: parameter (0 = static, 1 = dynamic1, 2 = dynamic2)
% Column 2: type of preconditioner: (0 = none, 1 = Jacobi, 2 = Gauss-Seidel)
setupExp = [0 0; 
            1 0; 
            2 0; 
            0 1; 
            1 1; 
            2 1; 
            0 2; 
            1 2; 
            2 2];

% Make struct
nExp = size(setupExp, 1);
nrIter = zeros(nExp,1);
for i = 1:nExp
    experiment{i}.aType = setupExp(i, 1);
    experiment{i}.pType = setupExp(i, 2);
    experiment{i}.alpha0 = 1;
end

% Variables
n = size(A,1);

% Zero initial guess
x0 = zeros(n,1);

% Parameters
tol = 1E-12;
maxIt = 10 * n;

% Perform experiments
for i = 1:nExp
    % Build the preconditioner
    experiment{i}.precon = makePrecon(A, experiment{i}.pType);

    % Compute alpha0 if needed
    if experiment{i}.aType == 0
        experiment{i}.alpha0 =...
            optimalAlpha(A, experiment{i}.precon, experiment{i}.pType);
    end

    % Perform experiment and save results
    [experiment{i}.x, experiment{i}.flag, experiment{i}.conv] =...
        iterMethod(A, b, x0, tol, maxIt,...
        experiment{i}.precon, experiment{i}.aType, experiment{i}.alpha0);
    nrIter(i) = length(experiment{i}.conv);
end

% Add CG for comparison
[experiment{nExp+1}.x, experiment{nExp+1}.flag, ~,~, experiment{nExp+1}.conv] =...
    pcg(A, b, tol, maxIt, [], [], x0);
experiment{nExp+1}.conv = experiment{nExp+1}.conv / norm(b);

% Display nr of iterations
precon = {'   P=I', 'Jacobi', '    GS'};
nlString = '+------+-------+-------+-------+\n';
fprintf(nlString);
fprintf('|      |static | dyn 1 | dyn 2 |\n');
fprintf(nlString);
for i = 1 : 3
    fprintf(['|', precon{i} ,'| %5.0f | %5.0f | %5.0f |\n'], [nrIter(3 * (i - 1) + 1 : 3 * i)]);
end
fprintf('|    CG| %5.0f |       |       |\n',length(experiment{nExp+1}.conv));
fprintf(nlString);

% Plot eigenvalues
figure(1)
myCmap = [0 0 0; 0 0 1; 1 0 0];
myStyle = {'.', 'o', 'x'};
eigTitle = {'No precon', 'Jacobi', 'Gauss-Seidel'};
hold on
for i = [1 2 3]
    plotEigvals(A, experiment{3 * (i - 1) + 1}.precon,...
        experiment{3 * (i - 1) + 1}.alpha0, myCmap(i, :), myStyle{i});
    phi = linspace(0, 2 * pi, 100); C = exp(1i * phi);
end
plot(real(C), imag(C), 'k');
hold off
legend([eigTitle, '|lambda| = 1']);
xlabel('Re(lambda)')
ylabel('Im(lambda)')
title('Spectrum of iteration matrix');

% Plot convergence
figure(2)
myCmap = hsv(4);
myStyle = {'-x', '-', '-', '-'};
semilogy(experiment{nExp + 1}.conv, 'k')
hold on
plotExp = [1 4 8 9];
for i = 1:4
    semilogy(experiment{plotExp(i)}.conv, myStyle{i}, 'Color', myCmap(i, :));
end
hold off
xlabel('Iteration')
ylabel('||r^{(k)}||/||b||')
title(sprintf('Relative residual, problem size: %.0f', n))
legend('CG', 'No precon, stat', 'Jacobi, stat', 'GS, dyn1', 'GS, dyn2', 'Location', 'NorthEast')
% saveFigure([], '../figures/iterMethods_convHist');

end

function P = makePrecon(A, pType)
    % Build preconditioner
    switch pType
        case 0
            P = [];
        case 1 
            % Jacobi
            P = diag(diag(A));
        case 2
            % Gauss-Seidel
            P = tril(A,0);
    end

end

function [alpha0] = optimalAlpha(A, P, pType)
    if ~isempty(P)
        % Preconditioner
        eigVal = eig(full(A), full(P));
    else
        % No preconditioner
        eigVal = eig(full(A));
    end
    [~, sorted] = sort(abs(eigVal));

    eigVal = eigVal(sorted);
    if (pType == 2)
        % Gauss-Seidel
        alpha0 = 3 / 2;
    else
        % Jacobi
        alpha0 = 2 / (eigVal(1) + eigVal(end));
    end

end

function [] = plotEigvals(A, P, alpha0, color, marker)
    if (~isempty(P))
        eigVal = eig(full(A), full(P)); 
    else
        eigVal = eig(full(A));
    end
    eigValShift = 1 - alpha0 * eigVal;

    plot(real(eigValShift), imag(eigValShift),...
        'Color', color, 'Marker', marker, 'LineStyle', 'none');
end