%!TEX root = ../report.tex
\section{Iterative methods}

\subsection{Preparation}

\subsubsection{Study things}
OK.

\subsubsection{Error relation}
We have an iterative method of the form:
\[
	\vec{x}^{(k+1)} = \vec{x}^{(k)} + \alpha_k\vec{z}^{(k)}, \text{ with } \vec{z}^{(k)} = \mat{P}^{-1}\vec{r}^{(k)}.
\]
The residual is given by
\[
	\vec{r}^{(k)} = \vec{b} - \mat{A}\vec{x}^{(k)}.
\]
The iteration matrix is given by
\[
	\mat{B_{\alpha_k}} = \mat{I} - \alpha_k\mat{P}^{-1}\mat{A}.
\]
The error in iteration \(k\) is defined as
\[
	\vec{e}^{(k)} = \vec{x} - \vec{x}^{(k)}.
\]
\(\mat{B_{\alpha_k}}\) is called the iteration matrix because
\[
	\vec{e}^{(k + 1)} = \mat{B_{\alpha_k}} \vec{e}^{(k)}.
\]
This can be seen as follows:
\begin{align*}
	\vec{e}^{(k + 1)}
		&= \vec{x} - \vec{x}^{(k + 1)} \\
		&= \vec{x} - \vec{x}^{(k)} - \alpha_k \vec{z}^{(k)} \\
		&= \vec{x} - \vec{x}^{(k)} - \alpha_k \mat{P}^{-1} \vec{r}^{(k)} \\
		&= \vec{x} - \vec{x}^{(k)} - \alpha_k \mat{P}^{-1} (\vec{b} - \mat{A} \vec{x}^{(k)}) \\
		&= \vec{x} - \vec{x}^{(k)} - \alpha_k \mat{P}^{-1} (\mat{A} \vec{x} - \mat{A} \vec{x}^{(k)}) \\
		&= \vec{x} - \alpha_k \mat{P}^{-1} \mat{A} \vec{x} - (\vec{x}^{(k)} - \alpha_k \mat{P}^{-1} \mat{A} \vec{x}^{(k)}) \\
		&= (I - \alpha_k \mat{P}^{-1} \mat{A})\vec{x} - (I - \alpha_k \mat{P}^{-1} \mat{A})\vec{x}^{(k)} \\
		&= \mat{B}_{\alpha_k} \vec{x} - \mat{B}_{\alpha_k}\vec{x}^{(k)} \\
		&= \mat{B}_{\alpha_k} (\vec{x} - \vec{x}^{(k)}) \\
		&= \mat{B}_{\alpha_k} \vec{e}^{(k)}.
\end{align*}

\subsubsection{Minimizing the error in the \texorpdfstring{\(\mat{A}\)}{A}-norm}
The error in the \(\mat{A}\)-norm squared is given by:
\begin{align*}
	\Phi_1(\alpha)
		&= \norm{\vec{e}^{(k + 1)}}_\mat{A}^2  \\
		&= \norm{\vec{x} - \vec{x}^{(k + 1)}}_\mat{A}^2 \\
		&= \norm{\vec{x} - \vec{x}^{(k)} - \alpha \vec{z}^{(k)}}_\mat{A}^2 \\
		&= \norm{\vec{e}^{(k)} - \alpha \vec{z}^{(k)}}_\mat{A}^2 \\
		&= \norm{\vec{e}^{(k)}}_\mat{A}^2 + \alpha^2 \norm{\vec{z}^{(k)}}_\mat{A}^2 - 2\alpha (\vec{z}^{(k)})^T \mat{A} \vec{e}^{(k)} \\
		&= \norm{\vec{e}^{(k)}}_\mat{A}^2 + \alpha^2 \norm{\vec{z}^{(k)}}_\mat{A}^2 - 2\alpha (\vec{z}^{(k)})^T \vec{r}^{(k)}. \\
\end{align*}
Its derivative with respect to \(\alpha\) is:
\[
	\Phi_1'(\alpha) = 2 \alpha \norm{\vec{z}^{(k)}}_\mat{A}^2 - 2 (\vec{z}^{(k)})^T \vec{r}^{(k)}.
\]
\(\Phi_1'(\alpha^1_k) = 0\) gives
\[
	2 \alpha^1_k \norm{\vec{z}^{(k)}}_\mat{A}^2 - 2 (\vec{z}^{(k)})^T \vec{r}^{(k)} = 0,
\]
from which follows
\[
	\alpha^1_k = \frac{
		(\vec{z}^{(k)})^T \vec{r}^{(k)}
	}{
		\norm{\vec{z}^{(k)}}_\mat{A}^2
	}
	= \frac{
		(\vec{z}^{(k)})^T \vec{r}^{(k)}
	}{
		(\vec{z}^{(k)})^T \mat{A} \vec{z}^{(k)}
	}.
\]

\subsubsection{\texorpdfstring{\(\mat{C}\)}{C}-norm of the error}
Assuming \(\mat{A}\) is symmetric, the squared 2-norm of the residual can be rewritten as follows:
\begin{align*}
	\norm{\vec{r}^{(k + 1)}}^2
		&= \norm{\mat{A}\vec{e}^{(k + 1)}}^2 \\
		&= (\mat{A} \vec{e}^{(k + 1)})^T \mat{A} \vec{e}^{(k + 1)} \\
		&= (\vec{e}^{(k + 1)})^T \mat{A}^T \mat{A} \vec{e}^{(k + 1)} \\
		&= \norm{\vec{e}^{(k + 1)}}_\mat{C}^2, \ \text{with} \ \mat{C}=\mat{A}^T\mat{A}.
\end{align*}

\subsubsection{2-norm of the residual}
The residual in the 2-norm squared is given by:
\begin{align*}
	\Phi_2(\alpha) 
		&= \norm{\vec{r}^{(k + 1)}}^2 \\
		&= \norm{\mat{A}\vec{e}^{(k + 1)}}^2 \\
		&= (\vec{e}^{(k + 1)})^T \mat{A}^T \mat{A} \vec{e}^{(k + 1)} \\
		&= \norm{\vec{e}^{(k + 1)}}_{\mat{A}^T \mat{A}}^2 \\
		&= \norm{\vec{e}^{(k)} - \alpha \vec{z}^{(k)}}_{\mat{A}^T \mat{A}}^2 \\
		&= \norm{\vec{e}^{(k)}}_{\mat{A}^T \mat{A}}^2 + \alpha^2 \norm{\vec{z}^{(k)}}_{\mat{A}^T \mat{A}}^2 - 2 \alpha (\vec{z}^{(k)})^T \mat{A}^T \mat{A} \vec{e}^{(k)}\\
		&= \norm{\vec{e}^{(k)}}_{\mat{A}^T \mat{A}}^2 + \alpha^2 \norm{\vec{z}^{(k)}}_{\mat{A}^T \mat{A}}^2 - 2 \alpha (\mat{A} \vec{z}^{(k)})^T \vec{r}^{(k)}.
\end{align*}
Minimizing with respect to \(\alpha\) yields
\[
	\Phi_2'(\alpha) = 2 \alpha \norm{\vec{z}^{(k)}}_{\mat{A}^T \mat{A}}^2 - 2 (\mat{A} \vec{z}^{(k)})^T \vec{r}^{(k)}.
\]
Solving \(\Phi_2'(\alpha^2_k) = 0\) gives:
\[
	\alpha^2_k 
		= \frac{
			(\mat{A} \vec{z}^{(k)})^T \vec{r}^{(k)}
		}{
			\norm{\vec{z}^{(k)}}_{\mat{A}^T \mat{A}}^2
		}
		= \frac{
			(\mat{A} \vec{z}^{(k)})^T \vec{r}^{(k)}
		}{
			(\mat{A} \vec{z}^{(k)})^T \mat{A} \vec{z}^{(k)}
		}.
\]

\subsubsection{Richardson iteration without preconditioning}
\(\mat{A}\) is a SPD matrix, and the static parameter \(\alpha\) is
\[
	\alpha = \frac{2}{\lambda_\text{max} + \lambda_\text{min}}.
\]
Since we use no preconditioning, \(\mat{P} = \mat{I}\). Hence
\[
	\mat{B}_\alpha = \mat{I} - \alpha \mat{P}^{-1} \mat{A} = \mat{I} - \alpha \mat{A},
\]
so \(\mat{B}_\alpha\) is also SPD, and therefore \emph{normal}. Therefore, we can obtain:
\[
	\norm{\vec{r}^{(k + 1)}} \leq \rho(\mat{B}_\alpha) \norm{\vec{r}^{(k)}}
\]
and by recursion:
\[
	\norm{\vec{r}^{(k)}} \leq \rho(\mat{B}_\alpha)^k \norm{\vec{r}^{(0)}}.
\]
The iteration can be stopped at iteration \(k\) if the residual is smaller than the given tolerance \(\epsilon\).
\[
	\norm{\vec{r}^{(k)}} \leq \epsilon \norm{\vec{b}}.
\]
\(k_\text{min}\) is the value for which the equality holds (combining the previous two equations):
\[
	\rho(\mat{B}_\alpha)^{k_\text{min}} \norm{\vec{r}^{(0)}} = \epsilon \norm{\vec{b}}.
\]
In other words:
\begin{align*}
	\rho(\mat{B}_\alpha)^{k_\text{min}} &=
	\frac{
		\epsilon \norm{\vec{b}}
	}{
		\norm{\vec{r}^{(0)}}
	} \\
	k_\text{min} = \log_{\rho(\mat{B}_\alpha)}{
		\frac{
			\epsilon \norm{\vec{b}}
		}{
			\norm{\vec{r}^{(0)}}
		}
	}
	&=
	\frac{
		\log\left(\frac{
			\epsilon \norm{\vec{b}}
		}{
			\norm{\vec{r}^{(0)}}
		}\right)
	}{
		\log(\rho(\mat{B}_\alpha))
	}.
\end{align*}
Moreover, for the spectral radius of \(\mat{B}_\alpha\) we have:
\begin{align*}
	\rho(\mat{B}_\alpha) &=
	\max\limits_i \abs{1 - \alpha \lambda_i} \\
	&=
	\max\limits_i \abs{
		1 - \frac{2 \lambda_i}{\lambda_\text{max} + \lambda_\text{min}}
	} \\
	&=
	\max\limits_i \abs{
		\frac{\lambda_\text{max} + \lambda_\text{min}}{\lambda_\text{max} + \lambda_\text{min}} - \frac{2 \lambda_i}{\lambda_\text{max} + \lambda_\text{min}}
	} \\
	&=
	\max\limits_i \abs{
		\frac{\lambda_\text{max} + \lambda_\text{min} - 2 \lambda_i}{\lambda_\text{max} + \lambda_\text{min}}
	} \\
	&=
	\abs{
		\frac{\lambda_\text{max} + \lambda_\text{min} - 2 \lambda_\text{min}}{\lambda_\text{max} + \lambda_\text{min}}
	} \\
	&=
	\frac{\lambda_\text{max} - \lambda_\text{min}}{\lambda_\text{max} + \lambda_\text{min}}.
\end{align*}