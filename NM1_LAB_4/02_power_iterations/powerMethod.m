% INPUT 
% A             n x n matrix 
% x0            n x 1 initial guess
% tol           desired tolerance
% maxIt         maximum number of iterations
% OUTPUT
% lambda        eigenvalue of A largest in magnitude
% x             corresponding eigenvector
% flag          if 0 then tolerance is attained
% lambdaList    list of intermediate eigenvalues
% xList         list of intermediate eigenvectors
% convHist      error estimate per iteration
function [lambda, x, flag, lambdaList, xList, convHist] =...
        powerMethod(A, x0, tol, maxIt)
    x_k = x0;
    xList = x_k;
    lambdaList = [];
    convHist = [];
    it = 0;
    y_k = x_k / norm(x_k);
    cont = true;
    if isa(A, 'function_handle')
    	mat_vec = A;
    else
    	mat_vec = @(x) A * x;
    end

    while cont

    	x_k = mat_vec(y_k);
    	xList = [xList, x_k];
    	lambda = y_k' * x_k;
    	y_k = x_k / norm(x_k);
    	lambdaList = [lambdaList, lambda];
    	if numel(lambdaList) >= 2
    		error_est = abs(lambdaList(end - 1) - lambdaList(end)) / abs(lambda);
    		convHist = [convHist, error_est];
    		cont = error_est > tol && it < maxIt;
    	else
    		cont = it < maxIt;
    	end
    	it = it + 1;
    end
    x = x_k;
    flag = it == maxIt;
end