close all; clear all;
N = 10;
A = gallery('poisson', N);
P_jacobi = diag(diag(A));
P_gss = tril(A, 0);

eigVal = eig(full(A), full(P_jacobi));
[~, sorted] = sort(abs(eigVal));
eigVal = eigVal(sorted);
alpha_jacobi = 2 / (eigVal(1) + eigVal(end));
alpha_gss = 3/2;

B_jacobi = eye(N*N) - alpha_jacobi * (P_jacobi \ A);
B_gss = eye(N*N) - alpha_gss * (P_gss \ A);

x_0 = rand(N*N, 1);
[lambda_jacobi, x_jacobi, flag_jacobi, lambdas_jacobi, xs_jacobi, convHist_jacobi] = powerMethod(B_jacobi, x_0, 1e-12, 10*N^2);
[lambda_gss, x_gss, flag_gss, lambdas_gss, xs_gss, convHist_gss] = powerMethod(B_gss, x_0, 1e-12, 10*N^2);

lambda_jacobi_ml = max(eigs(B_jacobi));
lambda_gss_ml = max(eigs(B_gss));
fprintf('Computed Jacobi eigenvalue: %f\nMatlab''s result: %f\n', lambda_jacobi, lambda_jacobi_ml);
fprintf('Computed Gauss-Seidel eigenvalue: %f\nMatlab''s result: %f\n', lambda_gss, lambda_gss_ml);

format long;

eigs_B_jacobi = sort(abs(eigs(B_jacobi)))
theoretical_conv_fact_jacobi = (eigs_B_jacobi(end - 1) / eigs_B_jacobi(end))^2

eigs_B_gss = sort(abs(eigs(B_gss)));
theoretical_conv_fact_gss = (eigs_B_gss(end - 1) / eigs_B_gss(end))


if flag_gss || flag_jacobi
	error('Not enough iterations.');
end

rel_err_jacobi = abs(lambda_jacobi - lambdas_jacobi) / abs(lambda_jacobi);
rel_err_gss = abs(lambda_gss - lambdas_gss) / abs(lambda_gss);

figure;
semilogy(1:numel(lambdas_jacobi), rel_err_jacobi);
hold on;
semilogy(1:numel(lambdas_gss), rel_err_gss);
xlabel('k');
ylabel('e^{(k)}');
legend('Jacobi', 'Gauss-Seidel');
title('Comparison of eigenvalue error');

% This formula comes from W. Kahan's advice in his paper "How Futile are Mindless Assessments of Roundoff in Floating-Point Computation?" 
% (https://www.cs.berkeley.edu/~wkahan/Mindless.pdf), section 12 "Mangled Angles."
angles_jacobi = zeros(1, size(xs_jacobi, 2));
for i=1:size(xs_jacobi, 2)
	angles_jacobi(i) = 2 * atan2(norm(norm(x_jacobi) * xs_jacobi(:, i) - norm(xs_jacobi(:, i)) * x_jacobi), norm(norm(x_jacobi) * xs_jacobi(:, i) + norm(xs_jacobi(:, i)) * x_jacobi));
end

angles_gss = zeros(1, size(xs_gss, 2));
for i=1:size(xs_gss, 2)
	angles_gss(i) = 2 * atan2(norm(norm(x_gss) * xs_gss(:, i) - norm(xs_gss(:, i)) * x_gss), norm(norm(x_gss) * xs_gss(:, i) + norm(xs_gss(:, i)) * x_gss));
end

figure;
semilogy(1:size(xs_jacobi, 2), sin(angles_jacobi));
hold on;
semilogy(1:size(xs_gss, 2), sin(angles_gss));
xlabel('k');
ylabel('sin(\theta)')
legend('Jacobi', 'Gauss-Seidel');
title('Comparison of eigenvectors');
