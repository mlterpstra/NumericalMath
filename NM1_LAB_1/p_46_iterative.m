tic;
a = 9; % The first odd composite number (after 1)
while true
	if ~isprime(a) % a is a composite number
		goldbach_right = false; % because we're super skeptical
		for b=1:sqrt(0.5 * a)
			if isprime(a - 2 * b * b)
				goldbach_right = true;
				break;
			end
		end
		if ~goldbach_right
			disp(['Goldbach was not right for ', num2str(a)]);
			break;
		end
	end
	a = a + 2;
end
toc;