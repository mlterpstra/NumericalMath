function [ primes ] = primes_upto( n )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
    possible_primes = 2:n;
    p = 2;
    idx = 1;
    while p*p < n
        new_list = [p, possible_primes(mod(possible_primes, p) ~= 0)];
        possible_primes = new_list;
        idx = idx + 1;
        p = possible_primes(idx);
    end
    primes = sort(possible_primes);
end

