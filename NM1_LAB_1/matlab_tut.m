odd_numbers = 9:2:6000;
odd_composites = odd_numbers(not(ismember(odd_numbers, primes_upto(6000))));
odd_composites(find(not(ismember(odd_composites, sum(combvec(primes_upto(6000), arrayfun(@(x) 2*x*x, 1:sqrt(6000))), 1))), 1 ))