% INPUT
% f         function of rootfinding problem
% df        function which is the derivative of f
% x_0        initial guess
% tol       desired tolerance
% maxIt     maximum number of iterations
% OUTPUT
% root      root of f
% flag      if 0: attained desired tolerance
%           if 1: reached maxIt nr of iterations
% convHist  convergence history
function [root, flag, convHist] = newtonParent(f, df, x_0, tol, maxIt)
	x = x_0;
	convHist = zeros(maxIt, 1);

	flag = 1;
	for it = 1 : maxIt
		x_next = phi(x, f, df);
		convHist(it) = abs(x_next - x);
		x = x_next;

		if convHist(it) < tol
			flag = 0;
			break;
		end
	end

	root = x;
	convHist(it + 1 : end) = [];
end

function x_next = phi(x, f, df)
	x_next = x - f(x) / df(x);
end