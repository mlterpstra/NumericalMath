% INPUT
% f         function of rootfinding problem
% a_0        static parameter: xnew = x + a0*f(x)
% x_0        initial guess
% tol       desired tolerance
% maxIt     maximum number of iterations
% OUTPUT
% root      root of f
% flag      if 0: attained desired tolerance
%           if 1: reached maxIt nr of iterations
% convHist  convergence history
function [root, flag, convHist] = staticParent(f, a_0, x_0, tol, maxIt)
	x = x_0;
	convHist = zeros(maxIt, 1);

	flag = 1;
	for it = 1 : maxIt
		x_next = phi(x, f, a_0);
		convHist(it) = abs(x_next - x);
		x = x_next;

		if convHist(it) < tol
			flag = 0;
			break;
		end
	end

	root = x;
	convHist(it + 1 : end) = [];
end

function x_next = phi(x, f, a_0)
	x_next = x + a_0 * f(x);
end