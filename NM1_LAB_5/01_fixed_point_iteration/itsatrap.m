close all;

f = @(x) log(x .* exp(x));
df = @(x) 1 / x + 1;

x_0 = 3;
a_0 = -1 / df(x_0); % (So that phi'(x_0) = 0)

phi_static = @(x) staticParent(f, a_0, x, 0, 1);
phi_newton = @(x) newtonParent(f, df, x, 0, 1);

tol = 1e-12;
maxIt = 50; % Assignment said 25, but static with 0 extrapolations needs more.

staticHists = cell(4, 1);
newtonHists = cell(4, 1);
for i = 1 : numel(staticHists)
	nrExtrap = i - 1;

	[~, flag, staticHists{i}] = aitkenExtrap(phi_static, x_0, tol, maxIt, nrExtrap);
	if flag
		warning('Static iteration with %d extrapolations not within tolerance!', nrExtrap);
	end

	[root, flag, newtonHists{i}] = aitkenExtrap(phi_newton, x_0, tol, maxIt, nrExtrap);
	if flag
		warning('Newton iteration with %d extrapolations not within tolerance!', nrExtrap);
	end
end

figure;
xs = 0:0.01:4;
plot(xs, real(f(xs)));
hold on;
plot([0, 4], [0, 0],'k');
grid on;
xlabel('x');
ylabel('f(x)');

figure;
for i = 1 : numel(staticHists)
	% Makes sure you don't try to plot log(0) in the logplot.
	if staticHists{i}(end) == 0
		staticHists{i}(end) = staticHists{i}(end) + eps(0);
	end
	semilogy(1:numel(staticHists{i}), staticHists{i});
	hold on;
end

xlabel('k');
ylabel('x_k - x_{k-1}');
legend('\phi_a', '\phi_{a, \Delta}^{(1)}', '\phi_{a, \Delta}^{(2)}', ...
	'\phi_{a, \Delta}^{(3)}');
grid on;
ylim([1e-13, 1e1]);
line([0, 30], [tol, tol], 'Color', 'k');

figure;
for i = 1 : numel(newtonHists)
	if newtonHists{i}(end) == 0
		newtonHists{i}(end) = newtonHists{i}(end) + eps(0);
	end
	semilogy(1:numel(newtonHists{i}), newtonHists{i});
	hold on;
end

xlabel('k');
ylabel('x_k - x_{k-1}');
legend('\phi_N', '\phi_{N, \Delta}^{(1)}', '\phi_{N, \Delta}^{(2)}', ...
	'\phi_{N, \Delta}^{(3)}');
grid on;
ylim([1e-13, 1e1]);
line([0, 30], [tol, tol], 'Color', 'k');