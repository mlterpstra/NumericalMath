close all;
lambda = 2;
sz = 3;
B = diag(lambda * ones(1, sz)) + diag(ones(1, sz - 1), 1);
x_0 = eye(sz);
tol = 1e-10;
maxIt = 25;

dfFunc_exact = @(x) matJac(x, 'exact', [], B);
[root_exact, flag_exact, iter_exact, convHist_exact] = newton(@(x) matFunc(x, B), dfFunc_exact, x_0, tol, maxIt);

exact_jacobian = dfFunc_exact(reshape(x_0, sz*sz, 1));
hs = zeros(1, 15);
for l=0:14
    h = 10 ^ -l;
    dfFunc_approx = @(x) matJac(x, 'approx', h, B);
    approx_jacobian = dfFunc_approx(reshape(x_0, sz*sz, 1));
    abs_diff = abs(approx_jacobian - exact_jacobian);
    hs(l + 1) = max(abs_diff(:));
end

l_opt = find(hs==min(hs)) - 1;
disp(['Best h value is ', num2str(10 ^ -l_opt), ' (error = ', num2str(min(hs)), ')']);
dfFunc_opt = @(x) matJac(x, 'approx', 10^-l_opt, B);
[root_opt, flag_opt, iter_opt, convHist_opt] = newton(@(x) matFunc(x, B), dfFunc_opt, x_0, tol, maxIt);
semilogy(1:iter_exact, convHist_exact);
hold on;
semilogy(1:iter_opt, convHist_opt);
num_elems = min(iter_opt, iter_exact);
semilogy(1:num_elems, abs(convHist_exact(1:num_elems) - convHist_opt(1:num_elems)));

dfFunc_fixed = @(x) matJac(x, 'approx', 0.1, B);
[root_fixed, flag_fixed, iter_fixed, convHist_fixed] = newton(@(x) matFunc(x, B), dfFunc_fixed, x_0, tol, maxIt);
semilogy(1:iter_fixed, convHist_fixed);

legend('Exact Jacobian', 'Optimal approximation', 'Difference optimal/exact', 'h=0.1');
title({'Error vs. iterations when computing matrix square root', ''});
xlabel('Iterations');
ylabel('Error est.');
