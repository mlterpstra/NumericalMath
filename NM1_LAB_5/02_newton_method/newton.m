% INPUT
% f         function of rootfinding problem
% df        function returning the Jacobian
% x_0        initial guess
% tol       desired tolerance
% maxIt     maximum number of iterations
% OUTPUT    
% root      if succesful, root is an approximation to the
%           root of the nonlinear equation
% flag      flag = 0: tolerance attained, flag = 1: reached maxIt
% iter      the number of iterations
% convHist  convergence history
function [root, flag, iter, convHist] = newton(f, df, x_0, tol, maxIt)
	x = reshape(x_0, numel(x_0), 1);
	convHist = zeros(maxIt, 1);

	flag = 1;
	for it = 1 : maxIt
		x_next = phi(x, f, df);
		convHist(it) = norm(x_next - x);
		x = x_next;

		if convHist(it) < tol
			flag = 0;
			iter = it;
			break;
		end
	end

	root = reshape(x, size(x_0));
	convHist(it + 1 : end) = [];
end

function x_next = phi(x, f, df)
	x_next = x - (df(x) \ f(x));
end