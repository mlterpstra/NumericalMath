close all; clear all;
load 'solarSimData.mat'

goback = false;
years_simulated = 5;  % set to 110 to see the next venus transit
if goback
	velAndPos(1:3, :) = -velAndPos(1:3, :);
end
vel_pos_reshaped = velAndPos(:);
du_at = @(t, u) nBodyF(t, u, 11, bodyMass);
h = 2^-12;
[t_arr_rk, sol_arr_rk] = odeSolveRK(du_at, [0, years_simulated], vel_pos_reshaped, 5, h);

sun   = sol_arr_rk(:, 4:6);
mercury = sol_arr_rk(:, 10:12);
venus = sol_arr_rk(:, 16:18);
earth = sol_arr_rk(:, 22:24);
moon  = sol_arr_rk(:, 28:30);

dme = moon - earth;
dms = moon - sun;

dmece = mercury - earth;
dmecs = mercury - sun;

dvene = venus - earth;
dvens = venus - sun;

tau = dot(dme, dms, 2) ./ (sqrt(dot(dme, dme, 2)) .* sqrt(dot(dms, dms, 2)));
tau_m = dot(dmece, dmecs, 2) ./ (sqrt(dot(dmece, dmece, 2)) .* sqrt(dot(dmecs, dmecs, 2)));
tau_v = dot(dvene, dvens, 2) ./ (sqrt(dot(dvene, dvene, 2)) .* sqrt(dot(dvens, dvens, 2)));

tol = 3e-4;

figure;
semilogy(t_arr_rk, abs(tau + 1));
hold on;
semilogy(t_arr_rk, abs(tau - 1));
legend('Solar eclipses', 'Lunar eclipses');
line(xlim, [tol, tol], 'Color', 'r');
xlabel('t (years)');
ylabel('|\tau \pm 1|');

solar = abs(tau + 1);
lunar = abs(tau - 1);
mec = abs(tau_m + 1);
ven = abs(tau_v + 1);
ccs_solar = bwlabel(solar < tol);
ccs_lunar = bwlabel(lunar < tol);
ccs_mec = bwlabel(mec < tol);
ccs_ven = bwlabel(ven < tol / 10);

next_two_solar_eclipses = [min(t_arr_rk(ccs_solar == 1)), min(t_arr_rk(ccs_solar == 2))];
next_two_lunar_eclipses = [min(t_arr_rk(ccs_lunar == 1)), min(t_arr_rk(ccs_lunar == 2))];
next_mercury_transits = [min(t_arr_rk(ccs_mec == 1)), min(t_arr_rk(ccs_mec == 2))];
next_venus_transits = [min(t_arr_rk(ccs_ven == 1)), min(t_arr_rk(ccs_ven == 2))];

disp('Next two solar eclipses:');
next_two_solar_eclipses = round(next_two_solar_eclipses * 365.25 * 24 * 3600);
for i=1:numel(next_two_solar_eclipses)
	t_star = next_two_solar_eclipses(i);
	if goback
		t_star = -t_star;
	end
	disp(datestr(addtodate(datenum('00:00 1-Jan-2016'), t_star, 'second'), 'HH:MM dddd dd mmmm yyyy'));
end

disp('Next two lunar eclipses:');
next_two_lunar_eclipses = round(next_two_lunar_eclipses * 365.25 * 24 * 3600);
for i=1:numel(next_two_lunar_eclipses)
	t_star = next_two_lunar_eclipses(i);
	if goback
		t_star = -t_star;
	end
	disp(datestr(addtodate(datenum('00:00 1-Jan-2016'), t_star, 'second'), 'HH:MM dddd dd mmmm yyyy'));
end

disp('Next two Mercury transits:');
next_mercury_transits = round(next_mercury_transits * 365.25 * 24 * 3600);
for i=1:numel(next_mercury_transits)
	t_star = next_mercury_transits(i);
	if goback
		t_star = -t_star;
	end
	disp(datestr(addtodate(datenum('00:00 1-Jan-2016'), t_star, 'second'), 'HH:MM dddd dd mmmm yyyy'));
end

disp('Next two Venus transits:');
next_venus_transits = round(next_venus_transits * 365.25 * 24 * 3600);
for i=1:numel(next_venus_transits)
	t_star = next_venus_transits(i);
	if goback
		t_star = -t_star;
	end
	disp(datestr(addtodate(datenum('00:00 1-Jan-2016'), t_star, 'second'), 'HH:MM dddd dd mmmm yyyy'));
end

simulateSolarSystem(t_arr_rk, sol_arr_rk, bodyData);