% INPUT
% t         current time (not used)
% solVec    current solution (should be 6x1 array)
% OUTPUT 
% dSolVec   right-hand side
function dSolVec = twoBodyF(t, solVec)
	half = numel(solVec) / 2;
	u = solVec(1:half, :);
	x = solVec(half + 1:end, :);

	dSolVec = [-((4*pi*pi) / (norm(x) ^ 3)) * x; u];
end