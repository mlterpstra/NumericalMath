% INPUT
% t         current time (not used)
% solVec    current solution (should be 6x1 array)
% OUTPUT 
% J         Jacobian matrix
function J = twoBodyJac(t, solVec)
	half = numel(solVec) / 2;
	x = solVec(half + 1:end, :);

	J = zeros(numel(solVec));
	B = -((4 * pi * pi) / (norm(x) ^ 3)) * eye(half) + ((12 * pi * pi) / (norm(x) ^ 5)) * (x * x');
	J(1:half, half + 1:end) = B;
	J(half + 1:end, 1:half) = eye(half);
end