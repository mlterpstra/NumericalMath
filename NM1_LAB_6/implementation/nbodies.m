close all; clear all;
warning('off','all'); % remove distracting warnings
R = 1;
P = 1;
omega = (2*pi) / (R*sqrt(R));
func = @(t, ux) twoBodyF(t, ux);
df = @(t, ux) twoBodyJac(t, ux);
x_exact = @(t) R * [cos(omega * t); sin(omega * t); 0];
u_exact = @(t) R * omega * [-sin(omega * t); cos(omega * t); 0];
ux_exact = @(t) [u_exact(t); x_exact(t)];
u0 = u_exact(0);
x0 = x_exact(0);
t_range = [0, 5*P];
i_range = 1:12;
num_methods = 5;
ts = zeros(num_methods, numel(i_range));
errors = zeros(num_methods, numel(i_range));
hs =  2 .^(-i_range) * P;
for i=i_range
	h = 2 ^ -i * P;
	fprintf('Step size %f\n', h);
	tic;
	[t_arr_rk, sol_arr_rk] = odeSolveRK(func, t_range, [u0; x0], 5, h);
	rk_time = toc;
	rk_exact = cell2mat(arrayfun(ux_exact, t_arr_rk', 'UniformOutput', false)); % WTF

	fprintf('5th-order Runge-Kutta      took %f seconds to solve\n', rk_time);
	tic;
	[t_arr_ab, sol_arr_ab] = odeSolveAB(func, t_range, [u0; x0], 5, h);
	ab_time = toc;
	ab_exact = cell2mat(arrayfun(ux_exact, t_arr_ab', 'UniformOutput', false)); % WTF
	fprintf('5th-order Adams-Bashforth  took %f seconds to solve\n', ab_time);
	tic;
	[t_arr_t0, sol_arr_t0] = odeSolveTheta(func, t_range, [u0; x0], df, 0, h);
	t0_time = toc;
	t0_exact = cell2mat(arrayfun(ux_exact, t_arr_t0', 'UniformOutput', false)); % WTF
	fprintf('Theta method (theta = 0)   took %f seconds to solve\n', t0_time);
	tic;
	[t_arr_thalf, sol_arr_thalf] = odeSolveTheta(func, t_range, [u0; x0], df, 0.5, h);
	thalf_time = toc;
	thalf_exact = cell2mat(arrayfun(ux_exact, t_arr_thalf', 'UniformOutput', false)); % WTF
	fprintf('Theta method (theta = 0.5) took %f seconds to solve\n', thalf_time);
	tic;
	[t_arr_t1, sol_arr_t1] = odeSolveTheta(func, t_range, [u0; x0], df, 1, h);
	t1_time = toc;
	t1_exact = cell2mat(arrayfun(ux_exact, t_arr_t1', 'UniformOutput', false)); % WTF
	fprintf('Theta method (theta = 1)   took %f seconds to solve\n', t1_time);
	ts(:, i) = [rk_time; ab_time; t0_time; thalf_time; t1_time];
	errors(:, i) = [sum(sqrt(sum((sol_arr_rk' - rk_exact).^2, 1)));
					sum(sqrt(sum((sol_arr_ab' - ab_exact).^2, 1)));
					sum(sqrt(sum((sol_arr_t0' - t0_exact).^2, 1)));
					sum(sqrt(sum((sol_arr_thalf' - thalf_exact).^2, 1)));
					sum(sqrt(sum((sol_arr_t1' - t1_exact).^2, 1)))];
	fprintf('\n');
end

% plot region
figure; 
loglog(hs, errors);
xlabel('Step size');
ylabel('Error');
xlim([min(hs), max(hs)])

l_1 = legend('RK', 'AB', '$\theta = 0$', '$\theta = \frac{1}{2}$', '$\theta = 1$', 'Location', 'SouthEast');
set(l_1,'Interpreter','latex')

figure;
loglog(hs, ts);
xlabel('Step size');
ylabel('Computation time');
xlim([min(hs), max(hs)])

l_2 = legend('RK', 'AB', '$\theta = 0$', '$\theta = \frac{1}{2}$', '$\theta = 1$', 'Location', 'SouthWest');
set(l_2,'Interpreter','latex')
