clear all; close all;
hs = 2e-2 + (-3:3) .* 1e-4;
lambda = -100;
tRange = [0, 10];
u_0 = 1;

f = @(t, u) lambda * (u - sin(t)) + cos(t);
df = @(t, u) lambda;
y_exact = @(t) exp(lambda * t) + sin(t);
thetas = [0, 1/2, 1];

%% IVP comparison first part
supercell = cell(numel(thetas), numel(hs), 2);
for i = 1:numel(thetas)
	for j=1:numel(hs)
		[tArray, solArray] = odeSolveTheta(f, tRange, u_0, df, thetas(i), hs(j));
		supercell{i, j, 1} = tArray;
		supercell{i, j, 2} = solArray;
	end
end

for i = 1:numel(thetas)
	figure;
	for j=1:numel(hs)
		subplot(2, 4, j);
		plot(supercell{i, j, 1}, supercell{i, j, 2});
		hold on; % HOLD THE DOOORRR! HOLLLDD THE DOOOOR!
		plot(supercell{i, j, 1}, y_exact(supercell{i, j, 1}));
		title(sprintf('h = %5.4f', hs(j)));
	end
	ax = subplot(2, 4, j + 1);
	text(0.5, 0.5, sprintf('\\theta = %2.1f', thetas(i)));
	set(ax, 'visible', 'off');
end

%% Part two
hs = 2 .^ (0:-1:-7);
lambda = -1;
tRange = [0, 10];
u_0 = 1;
f = @(t, u) lambda * (u - sin(t)) + cos(t);
df = @(t, u) lambda;

thetas = [0, 0.5, 1];
y_exact = @(t) exp(lambda * t) + sin(t);

comp_times = zeros(numel(thetas), numel(hs));
errs = zeros(numel(thetas), numel(hs));

for i=1:numel(thetas)
	for j=1:numel(hs)
		tic;
		[tArray, solArray] = odeSolveTheta(f, tRange, u_0, df, thetas(i), hs(j));
		comp_times(i, j) = toc;
		errs(i, j) = abs(y_exact(tArray(end)) - solArray(end));
	end
end

figure;
subplot(1, 2, 1);
loglog(hs, comp_times, 'o-');
title('Computation time vs. step-size');
xlabel('h');
ylabel('Time (s)');
xlim([min(hs), max(hs)])
l_1 = legend('$\theta = 0$', '$\theta = \frac{1}{2}$', '$\theta = 1$', 'Location', 'NorthWest');
set(l_1,'Interpreter','latex')

subplot(1, 2, 2);
loglog(hs, errs, 'o-');
title('Error vs. step-size');
xlabel('h');
ylabel('Error');
xlim([min(hs), max(hs)])
l_2 = legend('$\theta = 0$', '$\theta = \frac{1}{2}$', '$\theta = 1$', 'Location', 'NorthWest');
set(l_2,'Interpreter','latex')
