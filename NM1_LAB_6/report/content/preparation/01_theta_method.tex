%!TEX root = ../../report.tex
\subsection{The \texorpdfstring{\(\theta\)}{theta}-method}

\subsubsection{Study stuff}
OK.

\subsubsection{Explicit or implicit}
If \(\theta = 0\), we have
\[
	\vec{u}_{n+1} = \vec{u}_n + h \vec{f}_n.
\]
This is an explicit method, because the next \(\vec{u}_{n+1}\) is expressed in the values from the previous iteration.

If \(\theta = 1\), we have
\[
	\vec{u}_{n+1} = \vec{u}_n + h \vec{f}_{n+1}.
\]
This is an implicit method, because there is a \(\vec{f}_{n+1} = \vec{f}(t_{n+1}, \vec{u}_{n+1})\) on the right hand side.
To solve for \(\vec{u}_{n+1}\), you have to bring that \(\vec{u}_{n+1}\) to the left hand side, and may need to solve a nonlinear equation in the process.

\subsubsection{Rootfinding problem}
The general \(\theta\)-method is given by:
\[
	\vec{u}_{n+1} = \vec{u}_n + h \left(\theta \vec{f}_{n+1} + \left(1 - \theta\right) \vec{f}_n\right).
\]
This is equivalent to:
\begin{align*}
	\vec{u}_n + h \left(\theta \vec{f}_{n+1} + \left(1 - \theta\right) \vec{f}_n\right) - \vec{u} &= 0 \\
	\vec{u}_n + h \left(\theta \vec{f}(t_{n+1}, \vec{u}) + \left(1 - \theta\right) \vec{f}(t_n, \vec{u}_n)\right) - \vec{u} &= 0
\end{align*}
with \(\vec{u} = \vec{u}_{n+1}\), because that is what we want to solve for.

With \(\vec{F}(\vec{u})\) as the left hand side, we have:
\begin{align*}
	\vec{F}(\vec{u}) = 0.
\end{align*}

The Jacobian of \(\vec{F}\) is given by:
\begin{align*}
	\mat{J}(\vec{u}) &= \frac{\d}{\d \vec{u}}(\vec{F}(\vec{u})) \\
	&= \frac{\d}{\d \vec{u}} \vec{u}_n + h \frac{\d}{\d \vec{u}} \left(\theta \vec{f}(t_{n+1}, \vec{u}) + \left(1 - \theta\right) \vec{f}(t_n, \vec{u}_n)\right) - \frac{\d}{\d \vec{u}} \vec{u} \\
	&= 0 + h \left(\theta \frac{\d}{\d \vec{u}} \vec{f}(t_{n+1}, \vec{u}) + \left(1 - \theta\right) \frac{\d}{\d \vec{u}} \vec{f}(t_n, \vec{u}_n)\right) - \mat{I} \\
	&= h \left(\theta \hat{\mat{J}}(t_{n+1}, \vec{u}) + \left(1 - \theta\right) 0 \right) - \mat{I} \\
	&= \theta h \hat{\mat{J}}(t_{n+1}, \vec{u}) - \mat{I}.
\end{align*}

\subsubsection{The root condition}
Since the \(\theta\)-method is of the form as \cite[Eq. 8.23]{quarteroni2014}, we can determine the coeffictients \(\left\{a_k\right\}\), \(\left\{b_k\right\}\) and \(p\):
\[
	a_0 = 1, \quad b_{-1} = \theta, \quad b_0 = 1 - \theta, \quad p = 0.
\]

Therefore, the root polynomial is given by:
\begin{align*}
	\pi(r) &= r^{p + 1} - \sum\limits_{j = 0}^p a_j r^{p - j} \\
	&= r - a_0.
\end{align*}
Its root is:
\begin{align*}
	r_0 = a_0 = 1.
\end{align*}
Furthermore, \(\pi'(r_0) = 1 \neq 0\), so the root condition is satisfied.
Therefore, the method is zero-stable.

Also, \cite[Eq. 8.27]{quarteroni2014} holds, because of the coefficients:
\begin{align*}
	\sum\limits_{j=0}^p a_j &= 1 \\
	-\sum\limits_{j=0}^p j a_j + \sum\limits_{j=-1}^p b_j &= -0 + \theta + 1 - \theta = 1.
\end{align*}

Since the method is consistent and zero-stable we can conclude (by the Lax-Richtmyer equivalence theorem) that the method is convergent.

\subsubsection{Integration}
Integrating of the (one-dimensional) ODE over one step-size (\(h = t_{n+1} - t_n\)) yields
\begin{align*}
	u_{n+1} = u_n + \int\limits_{t_n}^{t_{n+1}} f(t, u(t))\d t.
\end{align*}

Applying the trapezoidal rule \cite[Eq. 4.19]{quarteroni2014} to approximate the integral yields:
\begin{align*}
	u_{n+1} &\approx u_n + \frac{t_{n+1} - t_n}{2}\left(f(t_{n+1}, u_{n+1}) + f(t_n, u_n)\right) \\
	&= \frac{h}{2}\left(f_{n+1} + f_n\right) \\
	&= h\left(\frac{1}{2} f_{n+1} + \frac{1}{2} f_n\right).
\end{align*}
This is the \(\theta\)-method with \(\theta = \frac{1}{2}\).
The trapezoidal rule has a local error of order \(\bigoh(h^3)\) (\cite[Eq. 4.20]{quarteroni2014}), so the same order holds for the local truncation error.

\subsubsection{\texorpdfstring{\theta}{theta}-method to the test problem}
The test problem states
\[
	f(t, u(t)) = \frac{\d u}{\d t} = \lambda u(t).
\]
Applying the \(\theta\)-method to this results in:
\begin{align*}
	u_{n+1} &= h\left( \theta f_{n+1} + (1 - \theta) f_n \right) \\
	&= h\left( \theta \lambda u_{n+1} + (1 - \theta) \lambda u_n \right).
\end{align*}
Rewriting this, we obtain
\[
	(1 - h \theta \lambda) u_{n+1} = u_n + h (1 - \theta) \lambda u_n.
\]
And finally
\[
	u_{n+1} = \frac{u_n + h (1 - \theta) \lambda u_n}{1 - h \theta \lambda} = \frac{1 + h(1 - \theta) \lambda}{1 - h \theta \lambda} u_n.
\]

\subsubsection{Unconditional stability}
We can write \(u_n\) as follows:
\[
	\left(
		\frac{
			1 + (1 - \theta) h \lambda
		}{
			1 - \theta h \lambda
		}
	\right)^n u_0
\]
The pre-factor tends to zero as \(n \rightarrow \infty\) for \(\theta \geq \frac{1}{2}\), for any \(h\).
Therefore it is unconditionaly absolutely stable.

\subsubsection{Exact solution}
Da real IVP is given by:
\[
	y'(t) = \lambda (y(t) - \sin(t)) + \cos(t), \quad y(0) = 1, \quad t \in \left[0, 10\right].
\]
The exact solution is \(y(t) = \euler^{\lambda t} + \sin(t)\), because in that case the LHS of the IVP is
\[
	y'(t) = \lambda \euler^{\lambda t} + \cos(t).
\]
And the RHS of the IVP is
\[
	\lambda (y(t) - \sin(t)) + \cos(t) = \lambda(\euler^{\lambda t} + \sin(t) - \sin(t)) + \cos(t) = \lambda\euler^{\lambda t} + \cos(t).
\]
This holds for all \(t\) and \(\lambda\).