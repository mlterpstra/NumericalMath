b = [1; 2];

fprintf('+---------+---------+---------+---------+---------+---------+---------+\n')
fprintf('| epsilon | condA   | A-LnUn  | condNaiv| condPivt| naiveErr| pivotErr|\n')
fprintf('+---------+---------+---------+---------+---------+---------+---------+\n')

for i=1:15
	epsilon = 10^(-i);
	A = [epsilon, 1; 1, 1];
	
	pivot = false;
	[xn, Ln, Un, Pn] = lu_decomp(A, b, pivot);
	
	pivot = true;
	[xp, Lp, Up, Pp] = lu_decomp(A, b, pivot);
	
	cond_number = cond(A);
	
	A_minus_LnUn = norm(A - Ln * Un);
	
	condNaiv = cond(Ln) * cond(Un);
	condPivt = cond(Lp) * cond(Up);
	
	x = A\b; % The 'exact' solution
	
	naiveErr = norm(x - xn) / norm(x);
	pivotErr = norm(x - xp) / norm(x);
	
	fprintf('| %7.1e | %7.1e | %7.1e | %7.1e | %7.1e | %7.1e | %7.1e |\n', ...
		epsilon, cond_number, A_minus_LnUn, condNaiv, condPivt, naiveErr, pivotErr);
end

fprintf('+---------+---------+---------+---------+---------+---------+---------+\n')
