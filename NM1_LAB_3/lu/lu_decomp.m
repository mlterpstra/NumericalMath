% INPUT
% A         square matrix
% b         right hand side
% OUTPUT    
% x         solution, such that A*x=b
% L         lower triangular matrix such that A = L*U
% U         upper triangular matrix such that A = L*U
function [x, L, U, P] = lu_decomp(A, b, pivot)
	[M, N] = size(A);
	if M ~= N
		disp('Error. A is not square!');
	end

	if nargin < 3
		pivot = false;
	end
	
	L = eye(N, N); % Diagonal of L is always one, so start with that
	U = zeros(N, N); % We will fill in the upper diagonal of U
	P = eye(N, N); % We will permute the permutation matrix

	% Eq. 5.13 (or Eq. 5.23 without pivoting)
	for k = 1:N - 1
		if pivot
			% Locate the max value in the kth column
			[~, r_bar] = max(abs(A(:, k)));
			
			% Switch the rows s.t. the max is on the diagonal
			A([k, r_bar], :) = A([r_bar, k], :);
			
			% Also for the permutation matrix
			P([k, r_bar], :) = P([r_bar, k], :);
		end
		
		U(k, k:N) = A(k, k:N);
		for i = k + 1:N
			L(i, k) = A(i, k) / A(k, k);
			A(i, k + 1:N) = A(i, k + 1:N) - L(i, k) * A(k, k + 1:N);
		end
	end
	% Also set last element of U
	U(end, end) = A(end, end);
	
	% Permute b in the same order as the rows in the matrix have been.
	% This makes sure that the solution x after substitution is the same,
	% and not permuted.
	b = P * b;
	
	y = zeros(N, 1);
	x = zeros(N, 1);
	
	% Forward substitution to solve Ly = b for y.
	y(1) = b(1);
	for i=2:N
		y(i) = b(i) - (dot(L(i, 1:i-1), y(1:i-1)));
	end
	
	% Backward substitution to solve Ux = y for x.
	x(N) = (1 / U(N, N)) * y(N);
	for i = N - 1:-1:1
		x(i) = (1/(U(i, i))) * (y(i) - dot(U(i, i + 1:N), x(i + 1:N)));
	end
end