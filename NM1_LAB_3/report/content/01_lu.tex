%!TEX root = ../report.tex
\section{LU factorisation and partial pivoting}
Quick recap of the terms mentioned:
\begin{description}
	\item[Overdetermined] A system \(A\vec{x}=\vec{b}\) with \(A \in \reals^{m \times n}\) is \emph{overdetermined} if \(m > n\).
	\item[Underdetermined] A system \(A\vec{x}=\vec{b}\) with \(A \in \reals^{m \times n}\) is \emph{underdetermined} if \(n > m\).
	\item[Inverse] The \emph{inverse} of a matrix \(A\) is the matrix \(A^{-1}\) s.t. \(AA^{-1} = A^{-1}A = I\).
	\item[Nonsingular] A \emph{nonsingular} matrix has an inverse.
	\item[Range] The \emph{range} of a matrix \(A\) is the span of the columns of \(A\):
	\[
		\text{range}(A) = \left\{ \vec{z} \in \reals^m \mid \vec{z} = A\vec{y} ,\ \forall \vec{y} \in \reals^n \right\}.
	\]
	\item[Nullspace] The \emph{nullspace} (also known as the kernel) of a matrix \(A\) is the subset of \(\reals^n\) that is mapped to \(\vec{0}\) by \(A\): 
	\[
		\text{ker}(A) = \left\{ \vec{y} \in \reals^n \mid A\vec{y} = \vec{0} \right\}.
	\]
	\item[Eigenvalue] An eigenvalue of a matrix \(A\) is a value \(\lambda\) for which \(A\vec{v}=\lambda\vec{v}\), where \(\vec{v}\) is one of \(A\)'s eigenvectors.
	\item[Norm] There are multiple matrix norms, but the \emph{2-norm} of a matrix \(A\) is defined as:
	\[
		\norm{A}_2 = \max_{\vec{x} \neq \vec{0}}\frac{\norm{A\vec{x}}_2}{\norm{\vec{x}}_2}.
	\]
	\item[Orthogonality] Vectors \(\vec{v}_1\) and \(\vec{v}_2\) are said to be \emph{orthogonal} if their inner product is zero: 
	\[
		\vec{v}_1 \cdot \vec{v}_2 = 0 \implies \vec{v}_1 \perp \vec{v}_2.
	\]
	A matrix \(A\) is called \emph{orthogonal} if its columns and rows are all orthogonal and unitary.
	In that case:
	\[
		A^{-1} = A^T.
	\]
\end{description}

\begin{figure}
\begin{lstlisting}
+---------+---------+---------+---------+---------+---------+---------+
| epsilon | condA   | A-LnUn  | condNaiv| condPivt| naiveErr| pivotErr|
+---------+---------+---------+---------+---------+---------+---------+
| 1.0e-01 | 3.0e+00 | 0.0e+00 | 9.3e+03 | 3.1e+00 | 3.1e-16 | 7.8e-17 |
| 1.0e-02 | 2.7e+00 | 0.0e+00 | 9.9e+07 | 2.7e+00 | 3.3e-15 | 1.8e-16 |
| 1.0e-03 | 2.6e+00 | 0.0e+00 | 1.0e+12 | 2.6e+00 | 1.6e-14 | 2.2e-16 |
| 1.0e-04 | 2.6e+00 | 0.0e+00 | 1.0e+16 | 2.6e+00 | 5.9e-13 | 1.8e-16 |
| 1.0e-05 | 2.6e+00 | 0.0e+00 | 1.0e+20 | 2.6e+00 | 5.2e-12 | 7.9e-17 |
| 1.0e-06 | 2.6e+00 | 0.0e+00 | 1.0e+24 | 2.6e+00 | 8.2e-11 | 1.8e-16 |
| 1.0e-07 | 2.6e+00 | 0.0e+00 | 1.0e+28 | 2.6e+00 | 3.6e-10 | 7.9e-17 |
| 1.0e-08 | 2.6e+00 | 0.0e+00 | 1.0e+32 | 2.6e+00 | 4.3e-09 | 1.8e-16 |
| 1.0e-09 | 2.6e+00 | 1.1e-16 | 1.0e+36 | 2.6e+00 | 9.9e-08 | 7.9e-17 |
| 1.0e-10 | 2.6e+00 | 0.0e+00 | 1.0e+40 | 2.6e+00 | 5.8e-08 | 0.0e+00 |
| 1.0e-11 | 2.6e+00 | 0.0e+00 | 1.0e+44 | 2.6e+00 | 5.8e-08 | 0.0e+00 |
| 1.0e-12 | 2.6e+00 | 0.0e+00 | 1.0e+48 | 2.6e+00 | 1.6e-05 | 2.2e-16 |
| 1.0e-13 | 2.6e+00 | 0.0e+00 | 1.0e+52 | 2.6e+00 | 2.2e-04 | 2.8e-16 |
| 1.0e-14 | 2.6e+00 | 0.0e+00 | 1.0e+56 | 2.6e+00 | 7.3e-03 | 0.0e+00 |
| 1.0e-15 | 2.6e+00 | 0.0e+00 | 1.0e+60 | 2.6e+00 | 7.8e-02 | 2.2e-16 |
+---------+---------+---------+---------+---------+---------+---------+
\end{lstlisting}
\end{figure}

\subsection{Why pivot?}
When not using pivoting, you get large values off the diagonal.
To solve the system, you have to compute with very large values.
This introduces numerical errors because the multipliers are too large.

Pivoting makes sure the largest value are on the diagonal and this causes a smaller numerical error.

\subsection{Time complexity of LU}
For a general matrix, the time complexity for LU decomposition is \(\bigoh(n^3)\).

For a tridiagonal matrix, the time complexity for LU decomposition is \(\bigoh(n)\).

\subsection{The subproblems}
\begin{itemize}
	\item 
	\(Ly = b\)
	\item
	\(Ux = y\)
\end{itemize}

\subsection{Epsilon to zero}

See question 1.
