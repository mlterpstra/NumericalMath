% INPUT 
% coeffs            exact coefficients
% x                 datapoints
% epsList           magnitude of the perturbation
function [pertNECoeff, pertQRCoeff] = leastSqComp(coeffs, x, epsList, make_plot)
	if nargin < 4
		make_plot = false;
	end
	% Exact coefficients
	coeffsNorm = norm(coeffs);
	y = polyval(coeffs, x);

	% Create Vandermonde matrix
	exactA = vander(x); exactA = exactA(:, end - length(coeffs) + 1 : end);
	exactAA = exactA' * exactA; condAA = cond(exactAA); normAA = norm(exactAA);

	% Compute economical QR factorisation
	[exactQ, exactR] = qr(exactA, 0);

	exactB = exactQ' * y;
	normExactB = norm(exactB);  % Precompute the norm of exactB

	% Precompute the 2-condition number and 2-norm of exactR
	condExactR = cond(exactR);
	normExactR = norm(exactR);

	% Make perturbations
	deltaX = -1 .^ (1 : size(y))';
	deltaY = deltaX;
	% Initialise arrays
	nrEps = length(epsList); 
	errorQRRel = zeros(nrEps, 1); errorQRBnd = zeros(nrEps, 1);
	errorNERel = zeros(nrEps, 1); errorNEBnd = zeros(nrEps, 1);

	pertQRCoeff = zeros(nrEps, numel(coeffs));
	pertNECoeff = zeros(nrEps, numel(coeffs));

	for i = 1 : nrEps
		% Set the maximum perturbation in x and y direction
		epsilonX = epsList(i); epsilonY = epsList(i);
		pertX = x + deltaX * epsilonX; % Perturb data
		pertY = y + deltaY * epsilonY;

		% Create Vandermonde matrix
		pertA = vander(pertX);
		pertA = pertA(:, end - length(coeffs) + 1 : end);
		deltaA = exactA - pertA;
		deltaAA = exactAA - pertA' * pertA;
		normDeltaAA = norm(deltaAA);

		% Compute economical QR factorisation
		[pertQ, pertR] = qr(pertA, 0);
		normDeltaR = norm(exactR - pertR);
		pertB = pertQ' * pertY; % The right-hand side
		deltaB = exactB - pertB;

		% Solve for perturbed data (by backward substitution)
		pertQRCoeff(i, :) = pertR \ pertB;
		pertNECoeff(i, :) = (pertA' * pertA) \ pertA' * pertY;
		
		% Compute relative errors (of the coefficients) and the corresponding
		% upper bounds
		errorQRRel(i) = norm(pertQRCoeff(i, :)' - coeffs) / coeffsNorm;
		errorNERel(i) = norm(pertNECoeff(i, :)' - coeffs) / coeffsNorm;
		
		% errorQRBnd(i) = cond(pertA) * (errorQRRel(i) / pertB);
		errorQRBnd(i) = condExactR * (norm(deltaB) / normExactB);
		
		errorNEBnd(i) = condAA * (normDeltaAA / normAA);
	end

	if make_plot
		loglog(epsList, errorQRRel, 'r')
		hold on
		loglog(epsList, errorQRBnd, 'r--')
		loglog(epsList, errorNERel, 'b')
		loglog(epsList, errorNEBnd, 'b--')
		hold off 
		xlabel('Perturbation')
		ylabel('Relative error')
		legend('QR', 'QR - bound', 'NE', 'NE - bound')
	end

end