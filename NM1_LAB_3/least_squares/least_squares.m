clear all;
close all;

g = 9.8133;
v_0 = 10;
theta_0 = pi / 4;

coeffs = trajectory(v_0, theta_0, g);
epslist = 10.^(0:-1:-16);

for n = [5, 25, 50]
	t = (0.3)/(n - 1) * (1:n)';
	x = cos(theta_0) * abs(v_0) * t;


	figure;
	leastSqComp(coeffs, x, epslist, true);
	title(sprintf('n = %d', n));
end

n = 5000;
t = (0.3)/(n - 1) * (1:n)';
x = cos(theta_0) * abs(v_0) * t;
[coeffsNE, coeffsQR] = leastSqComp(coeffs, x, 10^-15);
format long;
fprintf('%16.16f meter (NE)\n', -coeffsNE(1, 2) / coeffsNE(1, 1));
fprintf('%16.16f meter (QR)\n', -coeffsQR(1, 2) / coeffsQR(1, 1));
fprintf('%16.16f meter (exact)\n', -coeffs(2) / coeffs(1));

n = 20;
coeffs = (8:-1:0)';
x = linspace(0, 1, n)';
figure;
leastSqComp(coeffs, x, epslist, true);
title(sprintf('Higher order polynomial with n = %d', n));
