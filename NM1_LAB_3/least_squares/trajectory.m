function [ coeffs ] = trajectory(v_0, theta_0, g)
	coeffs = [-g / (v_0^2 + v_0^2 * cos(2 * theta_0)), tan(theta_0), 0]';
end
